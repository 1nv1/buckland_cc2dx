
-------------------------------------------------------------------------------

-- create the GoHome state

-------------------------------------------------------------------------------

-- cclog ("[Lua] Start\n")

-- ffi.cdef[[
--     int printf(const char *fmt, ...);
-- ]]

-- C = ffi.C

-- cclog("[Lua] cclog\n")

State = {}
State["GoHome"] = {}

State["GoHome"]["Enter"] = function(miner)

  cclog ("[Lua]: Walkin home in the hot n' thusty heat of the desert")

end


State["GoHome"]["Execute"] = function(miner)

  cclog ("[Lua]: Back at the shack. yer siree!")

  if miner:Fatigued() then

    miner:GetFSM():ChangeState(State["Sleep"])

  else

    miner:GetFSM():ChangeState(State["GoToMine"])

  end

end


State["GoHome"]["Exit"] = function(miner)

  cclog ("[Lua]: Puttin' mah boots on n' gettin' ready for a day at the mine")

end



-------------------------------------------------------------------------------

-- create the Sleep state

-------------------------------------------------------------------------------

-- State_Sleep = {}
State["Sleep"] = {}

State["Sleep"]["Enter"] = function(miner)

  cclog ("[Lua]: Miner "..miner:Name().." is dozin off")

end


State["Sleep"]["Execute"] = function(miner)

  if miner:Fatigued() then

    cclog ("[Lua]: ZZZZZZ... ")

    miner:DecreaseFatigue()

  else

    miner:GetFSM():ChangeState(State["GoToMine"])

  end

end

State["Sleep"]["Exit"] = function(miner)

  cclog ("[Lua]: Miner "..miner:Name().." is feelin' mighty refreshed!")

end


-------------------------------------------------------------------------------

-- create the GoToMine state

-------------------------------------------------------------------------------


-- State_GoToMine = {}
State["GoToMine"] = {}

State["GoToMine"]["Enter"] = function(miner)

  cclog ("[Lua]: Miner "..miner:Name().." enters goldmine")

end


State["GoToMine"]["Execute"] = function(miner)

  miner:IncreaseFatigue()

  miner:AddToGoldCarried(2)

  cclog ("[Lua]: Miner "..miner:Name().." has got "..miner:GoldCarried().." nuggets")


  if miner:GoldCarried() > 4 then

    cclog ("[Lua]: Miner "..miner:Name().." decides to go home, with his pockets full of nuggets")

    miner:GetFSM():ChangeState(State["GoHome"])

  end

end


State["GoToMine"]["Exit"] = function(miner)

  cclog ("[Lua]: Miner "..miner:Name().." exits goldmine")

  end




