State = {}
State.__index = State

function State:new(o)
    o = o or {}
    setmetatable(o, self)
    return o
end

function State:execute()
end

function State:enter()
end

function State:exit()
end


state_chase_ball = State:new()

state_chase_ball["enter"] = function(a_field_player)
    cclog ("[Lua]: enter chase_ball");
    a_field_player:Steering():SeekOn();
end

state_chase_ball["execute"] = function(a_field_player)
    -- if the ball is within kicking range the player changes state to KickBall.
    if a_field_player:BallWithinKickingRange() then
        a_field_player:GetFSM():ChangeState(state_kick_ball);

    elseif a_field_player:isClosestTeamMemberToBall() then
        a_field_player:Steering():SetTarget(a_field_player:Ball():Pos());

    else
        -- if the player is not closest to the ball anymore, he should return back
        -- to his home region and wait for another opportunity
        a_field_player:GetFSM():ChangeState(state_return_to_home_region);
    end
end

state_chase_ball["exit"] = function(a_field_player)
    a_field_player:Steering():SeekOff();
end

--------------------------------------------------------------------------------

state_dribble = State:new()

state_dribble["enter"] = function(a_field_player)
    cclog ("[Lua]: enter chase_ball");
    -- player->Team()->SetControllingPlayer(player);
    a_field_player:Team():SetControllingPlayer(a_field_player)
    -- player->setInfo(std::to_string(player->ID()) + " Dribble");
    a_field_player:setInfo(a_field_player:ID().." Dribble")
end

state_dribble["execute"] = function(a_field_player)
    local dot = a_field_player:Team():HomeGoal():Facing():Dot(a_field_player:Heading())

    -- if the ball is between the player and the home goal, it needs to swivel
    --  the ball around by doing multiple small kicks and turns until the player
    -- is facing in the correct direction
    if dot < 0 then
        -- the player's heading is going to be rotated by a small amount (Pi/4)
        -- and then the ball will be kicked in that direction
        local direction = a_field_player:Heading()

        -- calculate the sign (+/-) of the angle between the player heading and the
        -- facing direction of the goal so that the player rotates around in the
        -- correct direction
        local angle = QuarterPi * (-1) * a_field_player:Team():HomeGoal():Facing():Sign(a_field_player:Heading())

        Vec2DRotateAroundOrigin(direction, angle);

        -- this value works well whjen the player is attempting to control the
        -- ball and turn at the same time
        local KickingForce = 0.8;

        a_field_player:Ball():Kick(direction, KickingForce);
    -- kick the ball down the field
    else
        a_field_player:Ball():Kick(player:Team():HomeGoal():Facing(),
        NSSoccerWorld_Prm.MaxDribbleForce);
    end

    -- the player has kicked the ball so he must now change state to follow it
    a_field_player:GetFSM():ChangeState(state_chase_ball);
end

-- dribble["exit"] = function(a_field_player)

-- end

state_return_to_home_region["enter"] = function(a_field_player)
end

state_return_to_home_region["execute"] = function(a_field_player)
end