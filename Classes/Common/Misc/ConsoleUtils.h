#ifndef CONSOLE_UTILS_H
#define CONSOLE_UTILS_H
//------------------------------------------------------------------------
//
//  Name:   ConsoleUtils.h
//
//  Desc:   Just a few handy utilities for dealing with consoles
//
//  Author: Mat Buckland (fup@ai-junkie.com)
//
//------------------------------------------------------------------------
// #include <windows.h>
// #include <conio.h>
#include <iostream>

#define CON_RESET   "\033[0m"
#define CON_BLACK   "\033[30m"      /* Black */
#define CON_RED     "\033[31m"      /* Red */
#define CON_GREEN   "\033[32m"      /* Green */
#define CON_YELLOW  "\033[33m"      /* Yellow */
#define CON_BLUE    "\033[34m"      /* Blue */
#define CON_MAGENTA "\033[35m"      /* Magenta */
#define CON_CYAN    "\033[36m"      /* Cyan */
#define CON_WHITE   "\033[37m"      /* White */
#define CON_BOLD    "\033[1m"
#define CON_BOLDBLACK   "\033[1m\033[30m"      /* Bold Black */
#define CON_BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define CON_BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define CON_BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
#define CON_BOLDBLUE    "\033[1m\033[34m"      /* Bold Blue */
#define CON_BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
#define CON_BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */
#define CON_BOLDWHITE   "\033[1m\033[37m"      /* Bold White */

#define CON_FOREGROUND_BLUE                 CON_BLUE
#define CON_FOREGROUND_RED                  CON_RED
#define CON_FOREGROUND_GREEN                CON_GREEN
#define CON_FOREGROUND_YELLOW               CON_YELLOW

#define CON_BACKGROUND_RED                  "\033[41m"
#define CON_BACKGROUND_GREEN                "\033[42m"
#define CON_BACKGROUND_YELLOW               "\033[43m"
#define CON_BACKGROUND_BLUE                 "\033[44m"

#define CLEAR "\033[2J"  // clear screen escape code

//default text colors can be found in wincon.h
inline void SetTextColor(const std::string &_a_color)
{
  // HANDLE hConsole=GetStdHandle(STD_OUTPUT_HANDLE);

  // SetConsoleTextAttribute(hConsole, colors);
    std::cout << _a_color;
}

inline void PressAnyKeyToContinue()
{
  //change text color to white
  SetTextColor(CON_RESET);

  std::cout << "\n\nPress any key to continue" << std::endl;

  // while (!_kbhit()){}

  return;
}


#endif