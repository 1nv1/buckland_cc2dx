#ifndef COUNTER_H
#define COUNTER_H

#include <ctime>
#include <string>
#include <chrono>
#include <thread>

#include "Log.h"

namespace custom
{

class MyCounter
{
public:
    MyCounter()
    {
        name = "";
        tbeg = std::chrono::steady_clock::now();
    }

    MyCounter(std::string const &a_str) : name(a_str)
    {
        tbeg = std::chrono::steady_clock::now();
    }

    virtual ~MyCounter()
    {
        auto diff = std::chrono::steady_clock::now() - tbeg;
        printf("%.2f ms\n", std::chrono::duration <double, std::milli> (diff).count());
    }

    std::string name;
    std::chrono::steady_clock::time_point tbeg;

    static inline void sleep(uint32_t a_ms)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(a_ms));
    }

};

}

#define COUNTER_BEGIN                       { custom::MyCounter l_counter(std::to_string(__LINE__) + " : ")
#define COUNTER_BEGIN_STR(a_name)           { custom::MyCounter l_counter(std::to_string(__LINE__) + " : " + a_name)
#define COUNTER_END                         __LOGD("%s", l_counter.name.c_str()); }

#endif // COUNTER_H
