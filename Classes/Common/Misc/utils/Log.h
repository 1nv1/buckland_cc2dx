#ifndef LOG_H
#define LOG_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>

#ifdef LOG_EN

namespace custom {

class Log
{
public:
    enum class _ELvl
    {
        Init = 0,
        Info,
        Default = Info,
        Debug,
        Max,
    };

    static inline bool canLog(_ELvl const &alvl)
    {
        static char const *lPatterns[] = {"LOG_LVL", "LOGLVL", "LOGLEVEL", "LOG_LEVEL"};
        static char *lEnvi = nullptr;
        static _ELvl lLvl = _ELvl::Default;
        static bool lInit = false;

        if(!lInit)
        {
            lInit = true;
            for(char const *pPat : lPatterns)
            {
                lEnvi = std::getenv(pPat);
                if(lEnvi)
                {
                    lLvl = static_cast<_ELvl>(std::stoi(lEnvi));
                    break;
                }
            }
        }

        return (lLvl >= alvl);
    }
};

}

    #define __LOGW(format, ...) CCLOG("[W]%s %s [%d] " format "\n",__FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
    #define __PLOGW(format, ...) CCLOG("[W]%s %s p[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)getpid(), __LINE__, ##__VA_ARGS__ )
    #define __PPLOGW(format, ...) CCLOG("[W]%s %s pp[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)getppid(), __LINE__, ##__VA_ARGS__ )
    #define __TLOGW(format, ...) CCLOG("[W]%s %s t[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__ )

    #define __LOGE(format, ...) fprintf(stderr, "[E]%s %s [%d] " format ": %s\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(errno))
    #define __PLOGE(format, ...) fprintf(stderr, "[E]%s %s p[%d-%d] " format ": %s\n", __FILE__, __FUNCTION__, (int)getpid(), __LINE__, ##__VA_ARGS__, strerror(errno))
    #define __PPLOGE(format, ...) fprintf(stderr, "[E]%s %s pp[%d-%d] " format ": %s\n", __FILE__, __FUNCTION__, (int)getppid(), __LINE__, ##__VA_ARGS__, strerror(errno))
    #define __TLOGE(format, ...) fprintf(stderr, "[E]%s %s t[%d-%d] " format ": %s\n", __FILE__, __FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__, strerror(errno))

    #define __LOGI(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Info)) CCLOG("[I]%s %s [%d] " format "\n",__FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
    #define __PLOGI(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Info)) CCLOG("[I]%s %s p[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)getpid(), __LINE__, ##__VA_ARGS__ )
    #define __PPLOGI(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Info)) CCLOG("[I]%s %s pp[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)getppid(), __LINE__, ##__VA_ARGS__ )
    #define __TLOGI(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Info)) CCLOG("[I]%s %s t[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__ )

    #define __LOGD(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Debug)) CCLOG("[D]%s %s [%d] " format "\n",__FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
    #define __PLOGD(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Debug)) CCLOG("[D]%s %s p[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)getpid(), __LINE__, ##__VA_ARGS__ )
    #define __PPLOGD(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Debug)) CCLOG("[D]%s %s pp[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)getppid(), __LINE__, ##__VA_ARGS__ )
    #define __TLOGD(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Debug)) CCLOG("[D]%s %s t[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__ )

#else // LOG_EN

    #define __LOGW(...)
    #define __PLOGW(...)
    #define __PPLOGW(...)
    #define __TLOGW(...)
    #define __LOGE(...)
    #define __PLOGE(...)
    #define __PPLOGE(...)
    #define __TLOGE(...)

    #define __LOGI(...)
    #define __PLOGI(...)
    #define __PPLOGI(...)
    #define __TLOGI(...)

    #define __LOGD(...)
    #define __PLOGD(...)
    #define __PPLOGD(...)
    #define __TLOGD(...)

#endif // LOG_EN

#endif // LOG_H
