#ifndef __UTILS_H__
#define __UTILS_H__

#include <sstream>
#include <locale>
#include <cerrno>
#include <string>
#include <vector>

#include <cstring>
#include <cstdarg>

#include <sys/types.h>

#include <cocos/platform/CCPlatformConfig.h>

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include <process.h>
#define gettid                  GetCurrentThreadId
#else
#include <unistd.h>
#endif

#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()

static inline std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


static inline std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

#define BIT_GET(n)        (1 << (n))
#define BIT_SET(t, n)   (t | BIT_GET(n))
#define BIT_CLR(t, n)   (t & ~(BIT_GET(n)))

namespace custom
{
    template < typename T >
    inline std::string to_string( const T& t )
    {
        std::ostringstream stm ;
        stm << t ;
        return stm.str() ;
    }

    inline std::string &operator<<(std::string &astr, std::wstring const &awstr)
    {
        return astr = std::string(awstr.begin(), awstr.end());
    }

    inline std::wstring operator "" _ws(char const *astr, size_t)
    {
        std::string lstr(astr);
        return std::wstring(lstr.begin(), lstr.end());
    }
}




#define DIRECTOR (cocos2d::Director::getInstance())
#define RENDERER (DIRECTOR->getRenderer())

#define VISIBLE_SIZE (DIRECTOR->getVisibleSize())
#define ORIGIN (DIRECTOR->getVisibleOrigin())
#define WIN_SIZE (DIRECTOR->getWinSize())
#define WIN_SIZEPX (DIRECTOR->getWinSizeInPixels())

#define SCREEN_X(prop) ((VISIBLE_SIZE.width * (prop)) + (ORIGIN.x))
#define SCREEN_Y(prop) ((VISIBLE_SIZE.height * (prop)) + (ORIGIN.y))
#define SCREEN_SIZE(propX, propY)   cocos2d::Size(SCREEN_X(propX), SCREEN_Y(propY))
#define V2_SCREEN(propX, propY)     cocos2d::Vec2(SCREEN_X(propX), SCREEN_Y(propY))
#define V2_SCREEN_CENTER            V2_SCREEN(0.5, 0.5)

#define CONTENT_X(node_ptr, prop) ((node_ptr)->getContentSize().width * (prop))
#define CONTENT_Y(node_ptr, prop) ((node_ptr)->getContentSize().height * (prop))

#define V2_TOP_OF(parent, child) cocos2d::Vec2(CONTENT_X(parent, .5f), CONTENT_Y(parent, 1.f) - CONTENT_Y(child, .5f))
#define V2_TOP_LEFT_OF(parent, child) cocos2d::Vec2(CONTENT_X(child, .5f), CONTENT_Y(parent, 1.f) - CONTENT_Y(child, .5f))
#define V2_TOP_RIGHT_OF(parent, child) cocos2d::Vec2(CONTENT_X(parent, 1.f) - CONTENT_X(child, .5f), CONTENT_Y(parent, 1.f) - CONTENT_Y(child, .5f))
#define V2_CENTER_OF(parent, child) cocos2d::Vec2(CONTENT_X(parent, .5f), CONTENT_Y(parent, .5f))
#define V2_CENTER_LEFT_OF(parent, child) cocos2d::Vec2(CONTENT_X(child, .5f), CONTENT_Y(parent, .5f))
#define V2_CENTER_RIGHT_OF(parent, child) cocos2d::Vec2(CONTENT_X(parent, 1.f) - CONTENT_X(child, .5f), CONTENT_Y(parent, .5f))
#define V2_BOTTOM_OF(parent, child) cocos2d::Vec2(CONTENT_X(parent, .5f), CONTENT_Y(child, .5f))
#define V2_BOTTOM_LEFT_OF(parent, child) cocos2d::Vec2(CONTENT_X(child, .5f), CONTENT_Y(child, .5f))
#define V2_BOTTOM_RIGHT_OF(parent, child) cocos2d::Vec2(CONTENT_X(parent, 1.f) - CONTENT_X(child, .5f), CONTENT_Y(child, .5f))

#define V2_TOP_OF_PARENT(node_ptr) cocos2d::Vec2(CONTENT_X(node_ptr->getParent(), .5f), CONTENT_Y(node_ptr->getParent(), 1.f) - CONTENT_Y(node_ptr, .5f))
#define V2_TOP_LEFT_OF_PARENT(node_ptr) cocos2d::Vec2(CONTENT_X(node_ptr, .5f), CONTENT_Y(node_ptr->getParent(), 1.f) - CONTENT_Y(node_ptr, .5f))
#define V2_TOP_RIGHT_OF_PARENT(node_ptr) cocos2d::Vec2(CONTENT_X(node_ptr->getParent(), 1.f) - CONTENT_X(node_ptr, .5f), CONTENT_Y(node_ptr->getParent(), 1.f) - CONTENT_Y(node_ptr, .5f))
#define V2_CENTER_OF_PARENT(node_ptr) cocos2d::Vec2(CONTENT_X(node_ptr->getParent(), .5f), CONTENT_Y(node_ptr->getParent(), .5f))
#define V2_CENTER_LEFT_OF_PARENT(node_ptr) cocos2d::Vec2(CONTENT_X(node_ptr, .5f), CONTENT_Y(node_ptr->getParent(), .5f))
#define V2_CENTER_RIGHT_OF_PARENT(node_ptr) cocos2d::Vec2(CONTENT_X(node_ptr->getParent(), 1.f) - CONTENT_X(node_ptr, .5f), CONTENT_Y(node_ptr->getParent(), .5f))
#define V2_BOTTOM_OF_PARENT(node_ptr) cocos2d::Vec2(CONTENT_X(node_ptr->getParent(), .5f), CONTENT_Y(node_ptr, .5f))
#define V2_BOTTOM_LEFT_OF_PARENT(node_ptr) cocos2d::Vec2(CONTENT_X(node_ptr, .5f), CONTENT_Y(node_ptr, .5f))
#define V2_BOTTOM_RIGHT_OF_PARENT(node_ptr) cocos2d::Vec2(CONTENT_X(node_ptr->getParent(), 1.f) - CONTENT_X(node_ptr, .5f), CONTENT_Y(node_ptr, .5f))

#define V2_CONTENT(node_ptr, prop) cocos2d::Vec2(CONTENT_X(node_ptr, prop), CONTENT_Y(node_ptr, prop))
#define V2_CONTENT_CENTER(node_ptr) V2_CONTENT(node_ptr, 0.5)
#define V2_BOUNDING_CENTER(node_ptr) cocos2d::Vec2(BOUNDING_X(node_ptr, 0.5), BOUNDING_Y(node_ptr, 0.5))

#define SCREEN_SCALE_X(node_ptr) (SCREEN_X(1) / CONTENT_X(node_ptr, 1))
#define SCREEN_SCALE_Y(node_ptr) (SCREEN_Y(1) / CONTENT_Y(node_ptr, 1))

#define BOUNDING_X(node_ptr, prop) ((node_ptr)->getBoundingBox().size.width * (prop))
#define BOUNDING_Y(node_ptr, prop) ((node_ptr)->getBoundingBox().size.height * (prop))

#define VEC2_CHILD_POS_BY_TAG(parent, tag, func) ((parent)->func((parent)->getChildByTag(tag)->getPosition()))

// #ifndef __LOGI
//     #define __LOGI(format,...)            CCLOG("[I] %s[%s][%d]: " format "\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
// #endif
// #ifndef __LOGD
//     #define __LOGD(format,...)            CCLOG("[D] %s[%s][%d]: " format "\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
// #endif
// #ifndef __LOGE
//     #define __LOGE(format,...)            CCLOG("[E] %s[%s][%d]: " format " %s\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(errno))
// #endif
// #ifndef __LOGT
//     #define __LOGT(format,...)            CCLOG("[T'%d] %s[%s][%d]: " format "\n", gettid(), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
// #endif
// #ifndef __LOGP
//     #define __LOGP(format,...)            CCLOG("[P'%d] %s[%s][%d]: " format "\n", getpid(), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
// #endif

#define _FILE_DBG                   PRODUCT_NAME##".txt"

#define STR_LOGI(format,...)            __toStr("[I] %s[%s][%d]: " format "\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define STR_LOGD(format,...)            __toStr("[D] %s[%s][%d]: " format "\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define STR_LOGE(format,...)            __toStr("[E] %s[%s][%d]: " format "\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define STR_LOGT(format,...)            __toStr("[T'%d] %s[%s][%d]: " format "\n", gettid(), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define STR_LOGP(format,...)            __toStr("[P'%d] %s[%s][%d]: " format "\n", getpid(), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)

#define SLIDE_LENGTH        (SCREEN_X(.25f))

#define RUNNING_SCENE       (DIRECTOR->getRunningScene())

#define RUNNING_LAYER       (RUNNING_SCENE->getChildByTag(TAG_LAYER))

#define RUNNING_NODE        (RUNNING_LAYER ? RUNNING_LAYER : RUNNING_SCENE)

#define F_IS_EQUAL(aA, aB, aC)          ((aA) > ((aB) - (aC)) && (aA) < ((aB) + (aC)))

enum
{
    TAG_LAYER = 1000,
    TAG_LAYER_AP,
    TAG_LAYER_MAIN,
    TAG_LAYER_RECORD,
    TAG_LAYER_POPUP,
    TAG_LAYER_OVERLAY,
};

enum
{
    Z_01 = 100,
    Z_02 = 200,
    Z_03 = 300,
    Z_04 = 400,
    Z_MAX = 0x7FFFFFFF
};

#define REINTERPRET_CAST(val, type) (*reinterpret_cast<type const *>(&(val)))

#ifdef __OBJC__
#define OBJC_CLASS(name) @class name
#else
#define OBJC_CLASS(name) typedef struct objc_object name
#endif

#define PHYSBODYBYTAG(this, tag) (this)->getChildByTag(static_cast<int>(tag))->getPhysicsBody()

static inline std::string __toStr(const char *format, ...)
{
    int bufferSize = 0x100;
    char* buf = nullptr;
    va_list args;
    va_start(args, format);

    do
    {
        buf = new (std::nothrow) char[bufferSize];
        if (buf == nullptr)
            return std::string(); // not enough memory

        int ret = vsnprintf(buf, bufferSize - 3, format, args);
        if (ret < 0)
        {
            bufferSize *= 2;

            delete [] buf;
        }
        else
            break;

    } while (true);

    va_end(args);
    strcat(buf, "\0");
    return std::string(buf);
}

#define CREATE_FUNC_ARG01(__MAIN_TYPE__, __ARG_TYPE__1) \
static __MAIN_TYPE__* create(__ARG_TYPE__1 arg1) \
{ \
    __MAIN_TYPE__ *pRet = new(std::nothrow) __MAIN_TYPE__(arg1); \
    if (pRet && pRet->init()) \
    { \
        pRet->autorelease(); \
        return pRet; \
    } \
    else \
    { \
        delete pRet; \
        pRet = nullptr; \
        return nullptr; \
    } \
}

#define CREATE_FUNC_ARG02(__MAIN_TYPE__, __ARG_TYPE__1, __ARG_TYPE__2) \
static __MAIN_TYPE__* create(__ARG_TYPE__1 arg1, __ARG_TYPE__2 arg2) \
{ \
    __MAIN_TYPE__ *pRet = new(std::nothrow) __MAIN_TYPE__(arg1, arg2); \
    if (pRet && pRet->init()) \
    { \
        pRet->autorelease(); \
        return pRet; \
    } \
    else \
    { \
        delete pRet; \
        pRet = nullptr; \
        return nullptr; \
    } \
}

#endif // __UTILS_H__
