#include "Projectile_Bolt.h"
#include "../lua/Raven_Scriptor.h"
// #include "misc/cgdi.h"
#include "../Raven_Bot.h"
#include "../Raven_Game.h"
#include "../constants.h"
#include "Common/2D/WallIntersectionTests.h"
#include "../Raven_Map.h"

#include "../Raven_Messages.h"
#include "Common/Messaging/MessageDispatcher.h"


//-------------------------- ctor ---------------------------------------------
//-----------------------------------------------------------------------------
Bolt::Bolt(Raven_Bot* shooter, Vector2D target):

        Raven_Projectile(target,
                         shooter->GetWorld(),
                         shooter->ID(),
                         shooter->Pos(),
                         shooter->Facing(),
                         script->GetInt("Bolt_Damage"),
                         script->GetDouble("Bolt_Scale"),
                         script->GetDouble("Bolt_MaxSpeed"),
                         script->GetDouble("Bolt_Mass"),
                         script->GetDouble("Bolt_MaxForce"))
{
   assert (target != Vector2D());
}


//------------------------------ Update ---------------------------------------
//-----------------------------------------------------------------------------
void Bolt::Update()
{
  if (!m_bImpacted)
  {
    m_vVelocity = MaxSpeed() * Heading();

    //make sure vehicle does not exceed maximum velocity
    truncate(&m_vVelocity, m_dMaxSpeed);

    //update the position
    // m_vPosition += m_vVelocity;
    Vector2D l_pos = _position;
    l_pos += m_vVelocity;


    //if the projectile has reached the target position or it hits an entity
    //or wall it should explode/inflict damage/whatever and then mark itself
    //as dead


    //test to see if the line segment connecting the bolt's current position
    //and previous position intersects with any bots.
    Raven_Bot* hit = GetClosestIntersectingBot(l_pos - m_vVelocity,
                                               l_pos);

    //if hit
    if (hit)
    {
      m_bImpacted = true;
      m_bDead     = true;

      //send a message to the bot to let it know it's been hit, and who the
      //shot came from
      Dispatcher->DispatchMsg(SEND_MSG_IMMEDIATELY,
                              m_iShooterID,
                              hit->ID(),
                              Msg_TakeThatMF,
                              (void*)&m_iDamageInflicted);
    }

    //test for impact with a wall
    double dist;
     if( FindClosestPointOfIntersectionWithWalls(l_pos - m_vVelocity,
                                                 l_pos,
                                                 dist,
                                                 m_vImpactPoint,
                                                 m_pWorld->GetMap()->GetWalls()))
     {
       m_bDead     = true;
       m_bImpacted = true;

       l_pos = m_vImpactPoint;

       // return;
     }

      cocos2d::Node::setPosition(l_pos);
      cocos2d::Vec2 l_temp = m_vVelocity;
      float l_angle = CC_RADIANS_TO_DEGREES(atan2f(l_temp.x, l_temp.y));
      this->setRotation(l_angle);

      // __LOGD("%.2f %.2f", l_pos.x, l_pos.y);

      // m_dn_dbg->clear();
      // m_dn_dbg->drawLine(VectorToccVec2(l_pos), VectorToccVec2(m_vVelocity), cocos2d::Color4F(HEX_TO_C3B(GGC_RED_500)));
  }
}


//-------------------------- Render -------------------------------------------
//-----------------------------------------------------------------------------
void Bolt::Render()
{
  // gdi->ThickGreenPen();
  // gdi->Line(Pos(), Pos()-Velocity());
}