#ifndef ROCKETLAUNCHER_H
#define ROCKETLAUNCHER_H
#pragma warning (disable:4786)
//-----------------------------------------------------------------------------
//
//  Name:   RocketLauncher
//
//  Author: Mat Buckland (www.ai-junkie.com)
//
//  Desc:   class to implement a rocket launcher
//-----------------------------------------------------------------------------
#include "Raven_Weapon.h"



class  Raven_Bot;

class RocketLauncher : public Raven_Weapon
{
private:

  void     InitializeFuzzyModule();

public:

  RocketLauncher(Raven_Bot* owner);


  void Render();

  void ShootAt(Vector2D pos);

  double GetDesirability(double DistToTarget);

public:
    // from cc2dx
  static inline RocketLauncher *create(Raven_Bot* owner
                          ,const std::string &a_file="")
  {
      RocketLauncher *l_self = new (std::nothrow) RocketLauncher(owner);
      if (l_self && a_file != "" && l_self->init())
      {
          l_self->autorelease();
          l_self->setSprite(cocos2d::Sprite::create(a_file));
          return l_self;
      }
      CC_SAFE_DELETE(l_self);
      return nullptr;
  }

  using Raven_Weapon::m_sprite;

};



#endif