#ifndef BOLT_H
#define BOLT_H
#pragma warning (disable:4786)
//-----------------------------------------------------------------------------
//
//  Name:   Bolt.h
//
//  Author: Mat Buckland (ai-junkie.com)
//
//  Desc:   class to implement a bolt type projectile
//
//-----------------------------------------------------------------------------
#include "Raven_Projectile.h"

class Raven_Bot;



class Bolt : public Raven_Projectile
{
private:

  //tests the trajectory of the shell for an impact
  void TestForImpact();

public:

  Bolt(Raven_Bot* shooter, Vector2D target);

  void Render();

  void Update();


// from cc2dx
public:

    static inline Bolt *create(Raven_Bot* shooter, Vector2D target, const std::string &a_file="")
    {
      Bolt *l_self = new (std::nothrow) Bolt(shooter, target);
        if (l_self && a_file != "" && l_self->init())
        {
            l_self->autorelease();
            l_self->setSprite(cocos2d::Sprite::create(a_file));
            return l_self;
        }
        CC_SAFE_DELETE(l_self);
        return nullptr;
    }

    using BaseGameEntity::m_sprite;
    using BaseGameEntity::m_dn_dbg;
};


#endif