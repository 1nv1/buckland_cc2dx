#include "RavenWorld.h"

#include <Common/Base/RavenWorld/Raven_Game.h>

#include <cocos2d.h>

#include <Common/Debug/DBGLayer.h>
#include "Common/Misc/utils/Utils"

extern DBGLayer *_g_dbg_layer;

// static float _s_fps;
namespace NSRavenWorld {

RavenWorld::RavenWorld()
{
  // m_raven = new Raven_Game();

}

RavenWorld::~RavenWorld()
{
  // delete m_raven;
}

bool RavenWorld::init()
{
  if(!cocos2d::Scene::init()) return false;

  m_layer_action = cocos2d::Layer::create();
  this->addChild(m_layer_action);

  cocos2d::LayerColor *l_layer_color = cocos2d::LayerColor::create(cocos2d::Color4B(HEX_TO_C3B(GGC_BLUE_GREY_500)), SCREEN_X(1.f),SCREEN_Y(1.f));
  m_layer_action->addChild(l_layer_color, -1);

  // for(auto _l_vehicle : m_Vehicles)
  // {
  //   __LOGD("");
  //   m_layer_action->addChild(_l_vehicle);
  // }

  // m_dn_dbg = cocos2d::DrawNode::create();
  // this->addChild(m_dn_dbg, 1000);


  m_raven = Raven_Game::create();
  addChild(m_raven);

  scheduleUpdate();
  // schedule(CC_SCHEDULE_SELECTOR(RavenWorld::update), 0.5f);

  return true;
}

void RavenWorld::update(float a_dt)
{
  m_raven->Update();
}

}

cocos2d::Scene *getScene()
{
    return NSRavenWorld::RavenWorld::createScene();
}