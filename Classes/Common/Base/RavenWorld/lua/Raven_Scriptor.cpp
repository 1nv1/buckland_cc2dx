#include "Raven_Scriptor.h"

#include <cocos2d.h>

#include <Common/Misc/utils/Utils>

Raven_Scriptor* Raven_Scriptor::Instance()
{
  static Raven_Scriptor instance;

  return &instance;
}



Raven_Scriptor::Raven_Scriptor():Scriptor()
{
    RunScriptFile(cocos2d::FileUtils::getInstance()->fullPathForFilename("scripts/RavenWorld/Params.lua"));
}