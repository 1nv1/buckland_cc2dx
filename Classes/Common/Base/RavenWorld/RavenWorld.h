#ifndef __STEERING_WORLD_H__
#define __STEERING_WORLD_H__
#pragma warning (disable:4786)
//------------------------------------------------------------------------
//
//  Name:   RavenWorld.h
//
//  Desc:   All the environment data and methods for the Steering
//          Behavior projects. This class is the root of the project's
//          update and render calls (excluding main of course)
//
//  Author: Mat Buckland 2002 (fup@ai-junkie.com)
//
//------------------------------------------------------------------------
// #include <windows.h>
#include <vector>

#include <cocos2d.h>

#include <Common/Base/RavenWorld/Raven_Game.h>
#include "Common/Misc/utils/Utils"


namespace NSRavenWorld {

class RavenWorld : public cocos2d::Scene
{
public:
  RavenWorld();
  virtual ~RavenWorld();

public:
  // from cc2dx
  static inline cocos2d::Scene* createScene()
  {
    return RavenWorld::create();
  }

  // CREATE_FUNC(RavenWorld);
  static inline RavenWorld *create()
    {
        RavenWorld *ret = new (std::nothrow) RavenWorld();
        if (ret && ret->init())
        {
            ret->autorelease();
            return ret;
        }
        else
        {
            CC_SAFE_DELETE(ret);
            return nullptr;
        }
    }

    virtual bool init() override;

    virtual void update(float) override;

    cocos2d::DrawNode *m_dn_dbg;
    cocos2d::Layer *m_layer_action;
    Raven_Game *m_raven;
};

}

#endif