#include "Miner.h"

#include <Common/Misc/utils/Utils>

namespace NSWestWorld {

Miner *Miner::create(int _a_id)
{
    Miner *ret = new (std::nothrow) Miner(_a_id);
    if (ret && ret->init())
    {
        ret->autorelease();
        return ret;
    }
    else
    {
        CC_SAFE_DELETE(ret);
        return nullptr;
    }
}

bool Miner::HandleMessage(const Telegram& msg)
{
  return m_pStateMachine->HandleMessage(msg);
}


void Miner::update(float _a_dt)
{
  cocos2d::Node::update(_a_dt);
  // SetTextColor(FOREGROUND_RED| FOREGROUND_INTENSITY);
  SetTextColor(CON_YELLOW);

  m_iThirst += 1;

  m_pStateMachine->Update();
}



void Miner::AddToGoldCarried(const int val)
{
  m_iGoldCarried += val;

  if (m_iGoldCarried < 0) m_iGoldCarried = 0;
}

void Miner::AddToWealth(const int val)
{
  m_iMoneyInBank += val;

  if (m_iMoneyInBank < 0) m_iMoneyInBank = 0;
}

bool Miner::Thirsty()const
{
  if (m_iThirst >= ThirstLevel){return true;}

  return false;
}

bool Miner::Fatigued()const
{
  if (m_iFatigue > TirednessThreshold)
  {
    return true;
  }

  return false;
}

}