#include "WestWorld.h"
#include "SimpleAudioEngine.h"

#include <Common/Debug/DBGLayer.h>

#include <Common/Base/WestWorld/Miner.h>
#include <Common/Base/WestWorld/MinersWife.h>
#include <Common/Base/EntityManager.h>
#include <Common/Messaging/MessageDispatcher.h>
#include <Common/Base/WestWorld/EntityNames.h>

#include <Common/Misc/utils/Utils>

#include <cocos2d.h>

#include <cocos/base/ccFPSImages.h>

USING_NS_CC;

extern DBGLayer *_g_dbg_layer;


namespace NSWestWorld {

WestWorld::WestWorld(float _a_sec_interval) : cocos2d::Scene()
{
    _m_sec_interval = _a_sec_interval;
    m_sprite = nullptr;
    _m_cua = nullptr;
    _m_ghe = nullptr;
}

WestWorld::~WestWorld()
{
}

Scene* WestWorld::createScene(float _a_sec_interval /*= 100*/)
{
    return WestWorld::create(_a_sec_interval);
}

// on "init" you need to initialize your instance
bool WestWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto _l_visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 _l_origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto _l_closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(WestWorld::menuCloseCallback, this));

    _l_closeItem->setPosition(Vec2(_l_origin.x + _l_visibleSize.width - _l_closeItem->getContentSize().width/2 ,
                                _l_origin.y + _l_closeItem->getContentSize().height/2));

    // create menu, it's an autorelease object
    auto _l_menu = Menu::create(_l_closeItem, NULL);
    _l_menu->setPosition(Vec2::ZERO);
    this->addChild(_l_menu, 1);

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label

    auto _l_label = Label::createWithTTF("damn!", "fonts/Marker Felt.ttf", 24);

    // position the label on the center of the screen
    _l_label->setPosition(Vec2(_l_origin.x + _l_visibleSize.width/2,
                            _l_origin.y + _l_visibleSize.height - _l_label->getContentSize().height));

    // add the label as a child to this layer
    this->addChild(_l_label, 1);

    auto _l_listener = EventListenerCustom::create("director_reset", [&](EventCustom* event){
        __LOGI("director_reset");
        SetTextColor(CON_RESET);
    });

    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_l_listener, 1);

    // _m_cua = new Miner(ent_Miner_Bob);
    _m_cua = Miner::create(ent_Miner_Bob);
    this->addChild(_m_cua);
    // _m_ghe = new MinersWife(ent_Elsa);
    _m_ghe = MinersWife::create(ent_Elsa);
    this->addChild(_m_ghe);

    //register them with the entity manager
    EntityManager::Instance()->RegisterEntity(_m_cua);
    EntityManager::Instance()->RegisterEntity(_m_ghe);


    schedule(CC_SCHEDULE_SELECTOR(WestWorld::update), _m_sec_interval);
    // scheduleUpdate();


    _g_dbg_layer->setScale(3);
    static int _s_val = 0;
    _g_dbg_layer->push_back("oi1: ", &_s_val, DBGLayer::Type::Integer);
    _g_dbg_layer->setColor(cocos2d::Color3B::GREEN);
    _g_dbg_layer->push_back("oi2: ", &_s_val, DBGLayer::Type::Integer);


    return true;
}

// extern unsigned char cc_fps_images_png[];
// extern unsigned int cc_fps_images_len(void);

void WestWorld::onEnter()
{
    cocos2d::Scene::onEnter();

    __LOGD("0x%lx", DIRECTOR->getTextureCache()->getTextureForKey("/my_img"));

}


void WestWorld::menuCloseCallback(Ref* _a_sender)
{
    __LOGD("");
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif

    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);


}

void WestWorld::update(float _a_dt)
{
    __LOGD("");
    cocos2d::Scene::update(_a_dt);
    // *reinterpret_cast<DBGLayer *>(this->getChildByTag(1)) << std::to_string(_a_dt);
    _m_cua->update(_a_dt);
    _m_ghe->update(_a_dt);
    SetTextColor(CON_RESET);
    //dispatch any delayed messages
    MessageDispatcher::Instance()->DispatchDelayedMessages();
    // Sleep(800);
    // std::this_thread::sleep_for (std::chrono::milliseconds(50));

    *_g_dbg_layer << std::to_string(_a_dt);
}

}

cocos2d::Scene *getScene()
{
    return NSWestWorld::WestWorld::createScene();
}