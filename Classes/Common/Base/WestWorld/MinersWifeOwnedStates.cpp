#include "MinersWifeOwnedStates.h"
#include "MinerOwnedStates.h"
#include "MinersWife.h"
#include "Locations.h"
#include "Common/Time/CrudeTimer.h"
#include "Common/Messaging/MessageDispatcher.h"
#include "MessageTypes.h"
#include "EntityNames.h"

#include <iostream>
using std::cout;

#ifdef TEXTOUTPUT
#include <fstream>
extern std::ofstream os;
#define cout os
#endif

namespace NSWestWorld {
//-----------------------------------------------------------------------Global state

WifesGlobalState* WifesGlobalState::Instance()
{
  static WifesGlobalState instance;

  return &instance;
}


void WifesGlobalState::Execute(MinersWife* wife)
{
  //1 in 10 chance of needing the bathroom (provided she is not already
  //in the bathroom)
  if ( (RandFloat() < 0.1) &&
       !wife->GetFSM()->isInState(*VisitBathroom::Instance()) )
  {
    wife->GetFSM()->ChangeState(VisitBathroom::Instance());
  }
}

bool WifesGlobalState::OnMessage(MinersWife* wife, const Telegram& msg)
{
  // SetTextColor(BACKGROUND_RED|FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);
  SetTextColor(CON_RESET);
  // SetTextColor(CON_BACKGROUND_RED);

  switch(msg.Msg)
  {
  case Msg_HiHoneyImHome:
   {
       cout << "Message handled by " << GetNameOfEntity(wife->ID()) << " at time: "
       << Clock->GetCurrentTime() << std::endl;

     // SetTextColor(FOREGROUND_GREEN|FOREGROUND_INTENSITY);
       SetTextColor(CON_FOREGROUND_GREEN);

     cout << GetNameOfEntity(wife->ID()) <<
          ": Hi honey. Let me make you some of mah fine country stew" << std::endl;

     wife->GetFSM()->ChangeState(CookStew::Instance());
   }

   return true;

  }//end switch

  return false;
}

//-------------------------------------------------------------------------DoHouseWork

DoHouseWork* DoHouseWork::Instance()
{
  static DoHouseWork instance;

  return &instance;
}


void DoHouseWork::Enter(MinersWife* wife)
{
  cout << GetNameOfEntity(wife->ID()) << ": Time to do some more housework!" << std::endl;
}


void DoHouseWork::Execute(MinersWife* wife)
{
  switch(RandInt(0,2))
  {
  case 0:

    cout << GetNameOfEntity(wife->ID()) << ": Moppin' the floor" << std::endl;

    break;

  case 1:

    cout << GetNameOfEntity(wife->ID()) << ": Washin' the dishes" << std::endl;

    break;

  case 2:

    cout << GetNameOfEntity(wife->ID()) << ": Makin' the bed" << std::endl;

    break;
  }
}

void DoHouseWork::Exit(MinersWife* wife)
{
}

bool DoHouseWork::OnMessage(MinersWife* wife, const Telegram& msg)
{
  return false;
}

//------------------------------------------------------------------------VisitBathroom

VisitBathroom* VisitBathroom::Instance()
{
  static VisitBathroom instance;

  return &instance;
}


void VisitBathroom::Enter(MinersWife* wife)
{
  cout << GetNameOfEntity(wife->ID()) << ": Walkin' to the can. Need to powda mah pretty li'lle nose" << std::endl;
}


void VisitBathroom::Execute(MinersWife* wife)
{
  cout << GetNameOfEntity(wife->ID()) << ": Ahhhhhh! Sweet relief!" << std::endl;

  wife->GetFSM()->RevertToPreviousState();
}

void VisitBathroom::Exit(MinersWife* wife)
{
  cout << GetNameOfEntity(wife->ID()) << ": Leavin' the Jon" << std::endl;
}


bool VisitBathroom::OnMessage(MinersWife* wife, const Telegram& msg)
{
  return false;
}


//------------------------------------------------------------------------CookStew

CookStew* CookStew::Instance()
{
  static CookStew instance;

  return &instance;
}


void CookStew::Enter(MinersWife* wife)
{
  //if not already cooking put the stew in the oven
  if (!wife->Cooking())
  {
    cout << GetNameOfEntity(wife->ID()) << ": Putting the stew in the oven" << std::endl;

    //send a delayed message myself so that I know when to take the stew
    //out of the oven
    MessageDispatcher::Instance()->DispatchMsg(1.5,                  //time delay
                              wife->ID(),           //sender ID
                              wife->ID(),           //receiver ID
                              Msg_StewReady,        //msg
                              nullptr);

    wife->SetCooking(true);
  }
}


void CookStew::Execute(MinersWife* wife)
{
  cout << GetNameOfEntity(wife->ID()) << ": Fussin' over food" << std::endl;
}

void CookStew::Exit(MinersWife* wife)
{
  // SetTextColor(FOREGROUND_GREEN|FOREGROUND_INTENSITY);
  SetTextColor(CON_FOREGROUND_GREEN);

  cout << GetNameOfEntity(wife->ID()) << ": Puttin' the stew on the table" << std::endl;
}


bool CookStew::OnMessage(MinersWife* wife, const Telegram& msg)
{
  // SetTextColor(BACKGROUND_RED|FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);
  SetTextColor(CON_RESET);
  // SetTextColor(CON_BACKGROUND_RED);

  switch(msg.Msg)
  {
    case Msg_StewReady:
    {
      cout << "Message received by " << GetNameOfEntity(wife->ID()) <<
           " at time: " << Clock->GetCurrentTime() << std::endl;

      // SetTextColor(FOREGROUND_GREEN|FOREGROUND_INTENSITY);
      SetTextColor(CON_FOREGROUND_GREEN);
      cout << GetNameOfEntity(wife->ID()) << ": StewReady! Lets eat" << std::endl;

      //let hubby know the stew is ready
      MessageDispatcher::Instance()->DispatchMsg(SEND_MSG_IMMEDIATELY,
                                wife->ID(),
                                ent_Miner_Bob,
                                Msg_StewReady,
                                nullptr);

      wife->SetCooking(false);

      wife->GetFSM()->ChangeState(DoHouseWork::Instance());
    }

    return true;

  }//end switch

  return false;
}

}