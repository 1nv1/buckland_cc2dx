#ifndef __WEST_WORLD_H__
#define __WEST_WORLD_H__

#include "cocos2d.h"

#include <Common/Base/WestWorld/Miner.h>
#include <Common/Base/WestWorld/MinersWife.h>
#include <Common/Base/EntityManager.h>
#include <Common/Messaging/MessageDispatcher.h>

namespace NSWestWorld {

class WestWorld : public cocos2d::Scene
{
public:
    WestWorld(float _a_sec_interval);
    virtual ~WestWorld();

    static cocos2d::Scene* createScene(float _a_sec_interval = 0.1f);

    virtual bool init() override;

    virtual void update(float _a_dt) override;

    virtual void onEnter() override;

    // a selector callback
    void menuCloseCallback(cocos2d::Ref* _a_sender);

    // implement the "static create()" method manually
    // CREATE_FUNC(WestWorld);
    static inline WestWorld *create(float _a_sec_interval)
    {
        WestWorld *ret = new (std::nothrow) WestWorld(_a_sec_interval);
        if (ret && ret->init())
        {
            ret->autorelease();
            return ret;
        }
        else
        {
            CC_SAFE_DELETE(ret);
            return nullptr;
        }
    }

protected:
    cocos2d::Sprite *m_sprite;

    Miner *_m_cua;
    MinersWife *_m_ghe;

    float _m_sec_interval;
};

}
#endif // __WESTWORLD_H__
