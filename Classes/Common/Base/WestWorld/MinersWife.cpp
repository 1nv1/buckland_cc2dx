#include "MinersWife.h"

#include <Common/Misc/utils/Utils>

namespace NSWestWorld {

bool MinersWife::HandleMessage(const Telegram& msg)
{
  return m_pStateMachine->HandleMessage(msg);
}


void MinersWife::update(float _a_dt)
{
    cocos2d::Node::update(_a_dt);
  //set text color to green
  // SetTextColor(FOREGROUND_GREEN | FOREGROUND_INTENSITY);
  SetTextColor(CON_FOREGROUND_GREEN);

  m_pStateMachine->Update();
}

}