#include "MinerOwnedStates.h"
#include "Common/FSM/State.h"
#include "Miner.h"
#include "Locations.h"
#include "Common/Messaging/Telegram.h"
#include "Common/Messaging/MessageDispatcher.h"
#include "MessageTypes.h"
#include "Common/Time/CrudeTimer.h"
#include "EntityNames.h"

#include <iostream>
using std::cout;


#ifdef TEXTOUTPUT
#include <fstream>
extern std::ofstream os;
#define cout os
#endif

namespace NSWestWorld {
//------------------------------------------------------------------------methods for EnterMineAndDigForNugget
EnterMineAndDigForNugget* EnterMineAndDigForNugget::Instance()
{
  static EnterMineAndDigForNugget instance;

  return &instance;
}


void EnterMineAndDigForNugget::Enter(Miner* pMiner)
{
  //if the miner is not already located at the goldmine, he must
  //change location to the gold mine
  if (pMiner->Location() != goldmine)
  {
    cout << GetNameOfEntity(pMiner->ID()) << ": " << "Walkin' to the goldmine" << std::endl;

    pMiner->ChangeLocation(goldmine);
  }
}


void EnterMineAndDigForNugget::Execute(Miner* pMiner)
{
  //Now the miner is at the goldmine he digs for gold until he
  //is carrying in excess of MaxNuggets. If he gets thirsty during
  //his digging he packs up work for a while and changes state to
  //gp to the saloon for a whiskey.
  pMiner->AddToGoldCarried(1);

  pMiner->IncreaseFatigue();

  cout << GetNameOfEntity(pMiner->ID()) << ": " << "Pickin' up a nugget" << std::endl;

  //if enough gold mined, go and put it in the bank
  if (pMiner->PocketsFull())
  {
    pMiner->GetFSM()->ChangeState(VisitBankAndDepositGold::Instance());
  }

  if (pMiner->Thirsty())
  {
    pMiner->GetFSM()->ChangeState(QuenchThirst::Instance());
  }
}


void EnterMineAndDigForNugget::Exit(Miner* pMiner)
{
  cout << GetNameOfEntity(pMiner->ID()) << ": "
       << "Ah'm leavin' the goldmine with mah pockets full o' sweet gold" << std::endl;
}


bool EnterMineAndDigForNugget::OnMessage(Miner* pMiner, const Telegram& msg)
{
  //send msg to global message handler
  return false;
}

//------------------------------------------------------------------------methods for VisitBankAndDepositGold

VisitBankAndDepositGold* VisitBankAndDepositGold::Instance()
{
  static VisitBankAndDepositGold instance;

  return &instance;
}

void VisitBankAndDepositGold::Enter(Miner* pMiner)
{
  //on entry the miner makes sure he is located at the bank
  if (pMiner->Location() != bank)
  {
    cout << GetNameOfEntity(pMiner->ID()) << ": " << "Goin' to the bank. Yes siree" << std::endl;

    pMiner->ChangeLocation(bank);
  }
}


void VisitBankAndDepositGold::Execute(Miner* pMiner)
{
  //deposit the gold
  pMiner->AddToWealth(pMiner->GoldCarried());

  pMiner->SetGoldCarried(0);

  cout << GetNameOfEntity(pMiner->ID()) << ": "
       << "Depositing gold. Total savings now: "<< pMiner->Wealth() << std::endl;

  //wealthy enough to have a well earned rest?
  if (pMiner->Wealth() >= ComfortLevel)
  {
    cout << GetNameOfEntity(pMiner->ID()) << ": "
         << "WooHoo! Rich enough for now. Back home to mah li'lle lady" << std::endl;

    pMiner->GetFSM()->ChangeState(GoHomeAndSleepTilRested::Instance());
  }

  //otherwise get more gold
  else
  {
    pMiner->GetFSM()->ChangeState(EnterMineAndDigForNugget::Instance());
  }
}


void VisitBankAndDepositGold::Exit(Miner* pMiner)
{
  cout << GetNameOfEntity(pMiner->ID()) << ": " << "Leavin' the bank" << std::endl;
}


bool VisitBankAndDepositGold::OnMessage(Miner* pMiner, const Telegram& msg)
{
  //send msg to global message handler
  return false;
}
//------------------------------------------------------------------------methods for GoHomeAndSleepTilRested

GoHomeAndSleepTilRested* GoHomeAndSleepTilRested::Instance()
{
  static GoHomeAndSleepTilRested instance;

  return &instance;
}

void GoHomeAndSleepTilRested::Enter(Miner* pMiner)
{
  if (pMiner->Location() != shack)
  {
    cout << GetNameOfEntity(pMiner->ID()) << ": " << "Walkin' home" << std::endl;

    pMiner->ChangeLocation(shack);

    //let the wife know I'm home
    MessageDispatcher::Instance()->DispatchMsg(SEND_MSG_IMMEDIATELY, //time delay
                              pMiner->ID(),        //ID of sender
                              ent_Elsa,            //ID of recipient
                              Msg_HiHoneyImHome,   //the message
                              nullptr);
  }
}

void GoHomeAndSleepTilRested::Execute(Miner* pMiner)
{
  //if miner is not fatigued start to dig for nuggets again.
  if (!pMiner->Fatigued())
  {
     cout << GetNameOfEntity(pMiner->ID()) << ": "
          << "All mah fatigue has drained away. Time to find more gold!" << std::endl;

     pMiner->GetFSM()->ChangeState(EnterMineAndDigForNugget::Instance());
  }

  else
  {
    //sleep
    pMiner->DecreaseFatigue();

    cout << GetNameOfEntity(pMiner->ID()) << ": " << "ZZZZ... " << std::endl;
  }
}

void GoHomeAndSleepTilRested::Exit(Miner* pMiner)
{
}


bool GoHomeAndSleepTilRested::OnMessage(Miner* pMiner, const Telegram& msg)
{
   // SetTextColor(BACKGROUND_RED|FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);
  SetTextColor(CON_RESET);
  // SetTextColor(CON_BACKGROUND_RED);

   switch(msg.Msg)
   {
   case Msg_StewReady:

     cout << "Message handled by " << GetNameOfEntity(pMiner->ID())
     << " at time: " << Clock->GetCurrentTime() << std::endl;

     // SetTextColor(FOREGROUND_RED|FOREGROUND_INTENSITY);
     SetTextColor(CON_FOREGROUND_RED);

     cout << GetNameOfEntity(pMiner->ID())
          << ": Okay Hun, ahm a comin'!" << std::endl;

     pMiner->GetFSM()->ChangeState(EatStew::Instance());

     return true;

   }//end switch

   return false; //send message to global message handler
}

//------------------------------------------------------------------------QuenchThirst

QuenchThirst* QuenchThirst::Instance()
{
  static QuenchThirst instance;

  return &instance;
}

void QuenchThirst::Enter(Miner* pMiner)
{
  if (pMiner->Location() != saloon)
  {
    pMiner->ChangeLocation(saloon);

    cout << GetNameOfEntity(pMiner->ID()) << ": " << "Boy, ah sure is thusty! Walking to the saloon" << std::endl;
  }
}

void QuenchThirst::Execute(Miner* pMiner)
{
  pMiner->BuyAndDrinkAWhiskey();

  cout << GetNameOfEntity(pMiner->ID()) << ": " << "That's mighty fine sippin' liquer" << std::endl;

  pMiner->GetFSM()->ChangeState(EnterMineAndDigForNugget::Instance());
}


void QuenchThirst::Exit(Miner* pMiner)
{
  cout << GetNameOfEntity(pMiner->ID()) << ": " << "Leaving the saloon, feelin' good" << std::endl;
}


bool QuenchThirst::OnMessage(Miner* pMiner, const Telegram& msg)
{
  //send msg to global message handler
  return false;
}

//------------------------------------------------------------------------EatStew

EatStew* EatStew::Instance()
{
  static EatStew instance;

  return &instance;
}


void EatStew::Enter(Miner* pMiner)
{
  cout << GetNameOfEntity(pMiner->ID()) << ": " << "Smells Reaaal goood Elsa!" << std::endl;
}

void EatStew::Execute(Miner* pMiner)
{
  cout << GetNameOfEntity(pMiner->ID()) << ": " << "Tastes real good too!" << std::endl;

  pMiner->GetFSM()->RevertToPreviousState();
}

void EatStew::Exit(Miner* pMiner)
{
  cout << GetNameOfEntity(pMiner->ID()) << ": " << "Thankya li'lle lady. Ah better get back to whatever ah wuz doin'" << std::endl;
}


bool EatStew::OnMessage(Miner* pMiner, const Telegram& msg)
{
  //send msg to global message handler
  return false;
}

}
