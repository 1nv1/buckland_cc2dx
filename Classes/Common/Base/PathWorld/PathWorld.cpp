#include "PathWorld.h"
#include "SimpleAudioEngine.h"

#include <Common/Debug/DBGLayer.h>
#include <Common/Misc/utils/Utils>

// #include <Common/Base/PathWorld/ParamLoader.h>

#include <cocos2d.h>

extern DBGLayer *_g_dbg_layer;

USING_NS_CC;
namespace NSPathWorld
{

PathWorld::PathWorld(int cx, int cy) : cocos2d::Scene()
{
    _m_dbg_draw = nullptr;
    // _m_layer_action = nullptr;
    // ParamLoader* p = ParamLoader::Instance();
}

PathWorld::~PathWorld()
{
}

// on "init" you need to initialize your instance
bool PathWorld::init()
{
    if(!cocos2d::Scene::init()) return false;

    // _m_layer_action = cocos2d::Layer::create();
    // this->addChild(_m_layer_action);

    // cocos2d::LayerColor *_l_layer_color = cocos2d::LayerColor::create(cocos2d::Color4B::WHITE, SCREEN_X(1.f),SCREEN_Y(1.f));
    // _m_layer_action->addChild(_l_layer_color, 0);

    _m_dbg_draw = cocos2d::DrawNode::create();
    this->addChild(_m_dbg_draw, 1000);

    scheduleUpdate();
    // schedule(CC_SCHEDULE_SELECTOR(SteeringWorld::update), 3.f);

    __LOGD("");
    return true;
}

// extern unsigned char cc_fps_images_png[];
// extern unsigned int cc_fps_images_len(void);

void PathWorld::onEnter()
{
    cocos2d::Scene::onEnter();

}


void PathWorld::update(float _a_dt)
{

    cocos2d::Scene::update(_a_dt);
    // _m_soccer_pitch->update(_a_dt);

    // *_g_dbg_layer << std::to_string(_a_dt);
}

}

cocos2d::Scene *getScene()
{
    return NSPathWorld::PathWorld::createScene();
}