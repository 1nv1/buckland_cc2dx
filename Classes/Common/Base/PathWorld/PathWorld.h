#ifndef __PATH_WORLD_H__
#define __PATH_WORLD_H__

#include "cocos2d.h"

#include <Common/Misc/utils/Utils>

// #include <Common/Base/PathWorld/SoccerPitch.h>

namespace NSPathWorld
{

class PathWorld : public cocos2d::Scene
{
public:
    PathWorld(int cx, int cy);
    virtual ~PathWorld();

    // static cocos2d::Scene* createScene(float _a_sec_interval = 0.1f);

    virtual bool init() override;

    virtual void update(float _a_dt) override;

    virtual void onEnter() override;

protected:

public:
  // from cc2dx
  static inline cocos2d::Scene* createScene()
  {
    return PathWorld::create(static_cast<int>(SCREEN_X(1.f)), static_cast<int>(SCREEN_Y(1.f)));
  }

  // CREATE_FUNC(PathWorld);
  static inline PathWorld *create(int cx, int cy)
    {
        PathWorld *ret = new (std::nothrow) PathWorld(cx, cy);
        if (ret && ret->init())
        {
            ret->autorelease();
            return ret;
        }
        else
        {
            CC_SAFE_DELETE(ret);
            return nullptr;
        }
    }

    // virtual bool init() override;

    cocos2d::DrawNode *_m_dbg_draw;
    cocos2d::Layer *_m_layer_action;
};

}
#endif // __PATH_WORLD_H__
