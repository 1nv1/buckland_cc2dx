 #ifndef BASE_GAME_ENTITY_H
#define BASE_GAME_ENTITY_H
#pragma warning (disable:4786)
//------------------------------------------------------------------------
//
//  Name: BaseGameEntity.h
//
//  Desc: Base class to define a common interface for all game
//        entities
//
//  Author: Mat Buckland (fup@ai-junkie.com)
//
//------------------------------------------------------------------------
#include <vector>
#include <string>
#include <iosfwd>
#include "Common/2D/Vector2D.h"
#include "Common/2D/Geometry.h"
#include "Common/Misc/Utils.h"

#include <cocos2d.h>


struct Telegram;

class BaseGameEntity : public virtual cocos2d::Node
{
public:

  enum {default_entity_type = -1};

private:

  //each entity has a unique ID
  int         m_ID;

  //every entity has a type associated with it (health, troll, ammo etc)
  int         m_iType;

  //this is a generic flag.
  bool        m_bTag;

  //this is the next valid ID. Each time a BaseGameEntity is instantiated
  //this value is updated
  static int  m_iNextValidID;

  //this must be called within each constructor to make sure the ID is set
  //correctly. It verifies that the value passed to the method is greater
  //or equal to the next valid ID, before setting the ID and incrementing
  //the next valid ID
  void SetID(int val);


protected:

  //its location in the environment
  // Vector2D m_vPosition;

  // Vector2D m_vScale;

  //the magnitude of this object's bounding radius
  double    m_dBoundingRadius;

  BaseGameEntity(int ID);

  BaseGameEntity() :
    m_ID(GetNextValidID()),
    m_dBoundingRadius(0.0),
    m_iType(default_entity_type),
    m_bTag(false),
    m_sprite(nullptr),
    m_info(nullptr),
    m_dn_dbg(nullptr),
    Node()
  {}

  BaseGameEntity(int ID, const Vector2D &a_pos, float _a_rad) :
    m_dBoundingRadius(0.0),
    m_iType(default_entity_type),
    m_bTag(false),
    m_sprite(nullptr),
    m_info(nullptr),
    m_dn_dbg(nullptr),
    Node()
  {
    SetID(ID);
    cocos2d::Node::setPosition(a_pos);
    m_dBoundingRadius = _a_rad;
    cocos2d::Node::setContentSize(cocos2d::Size(m_dBoundingRadius, m_dBoundingRadius));
  }

public:

  virtual ~BaseGameEntity(){}

  // virtual void Update(){};

  // virtual void Render()=0;

  virtual bool HandleMessage(const Telegram& msg){return false;}

  //entities should be able to read/write their data to a stream
  virtual void Write(std::ostream&  os)const{}
  virtual void Read (std::ifstream& is){}

  //use this to grab the next valid ID
  static int   GetNextValidID(){return m_iNextValidID;}

  //this can be used to reset the next ID
  static void  ResetNextValidID(){m_iNextValidID = 0;}



  Vector2D     Pos()const{return _position;}
  void         SetPos(const Vector2D &new_pos){ cocos2d::Node::setPosition(new_pos);}

  double       BRadius()const{return m_dBoundingRadius;}
  void         SetBRadius(double r){m_dBoundingRadius = r;}
  int          ID()const{return m_ID;}

  bool         IsTagged()const{return m_bTag;}
  void         Tag(){m_bTag = true;}
  void         UnTag(){m_bTag = false;}

  // Vector2D     Scale()const{return m_vScale;}
  // void         SetScale(Vector2D val){m_dBoundingRadius *= MaxOf(val.x, val.y)/MaxOf(m_vScale.x, m_vScale.y); m_vScale = val;}
  void         SetScale(double val){m_dBoundingRadius *= (val/MaxOf(_scaleX, _scaleY)); cocos2d::Node::setScale(val);}

  int          EntityType()const{return m_iType;}
  void         SetEntityType(int new_type){m_iType = new_type;}

public:
  // from cc2dx
  cocos2d::Sprite *m_sprite;
  cocos2d::LabelAtlas *m_info;
  cocos2d::DrawNode *m_dn_dbg;

  virtual inline void setColor(const cocos2d::Color3B& color) override
  {
    if(m_sprite)
    {
      m_sprite->setColor(color);
    }
  }

  virtual inline void setRotation(float rotation) override
  {
    if(m_sprite)
    {
      m_sprite->setRotation(rotation);
    }
  }

  virtual inline bool init() override
  {
    if(!cocos2d::Node::init()) return false;
    this->setAnchorPoint(cocos2d::Vec2::ANCHOR_MIDDLE);
    m_info = cocos2d::LabelAtlas::create();
    m_info->initWithString("", DIRECTOR->getTextureCache()->getTextureForKey("/dbg_font"), 12, 32 , '.');
    addChild(m_info , 0x1000);

    m_dn_dbg = cocos2d::DrawNode::create();
    addChild(m_dn_dbg, 0x1000 - 1);

    return true;
  }

  virtual inline void setInfo(const std::string &_a_str)
  {
    if(m_info) m_info->setString(_a_str);
  }

  virtual inline void setSprite(cocos2d::Sprite *_a_sprite)
  {

    if(m_sprite)
    {
      m_sprite->removeFromParent();
    }

    m_sprite = _a_sprite;
    this->addChild(m_sprite, 0x100, 0x100);
    setContentSize(m_sprite->getContentSize());
    m_sprite->setPosition(V2_CENTER_OF_PARENT(m_sprite));

    m_info->setPosition(V2_TOP_OF_PARENT(m_info));
    m_dBoundingRadius = getBoundingBox().size.width;
  }

  virtual inline void drawBoundingBox(const cocos2d::Color4F &_a_color = cocos2d::Color4F(HEX_TO_C3B(GGC_INDIGO_300)))
  {

    if(!m_dn_dbg) return;

    m_dn_dbg->clear();
    m_dn_dbg->drawRect(cocos2d::Vec2::ZERO, cocos2d::Vec2(this->getContentSize().width, this->getContentSize().height), _a_color);
  }

  virtual inline void setScale(float _a_scale) override
  {
    cocos2d::Node::setScale(_a_scale);
    m_dBoundingRadius = getBoundingBox().size.width;
  }

};

#endif




