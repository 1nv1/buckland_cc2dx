#define PLAYER_STATE_INFO_ON 1
#include "SoccerWorld.h"
#include "SimpleAudioEngine.h"

#include <Common/Debug/DBGLayer.h>
#include <Common/Misc/utils/Utils>

#include <Common/Base/SoccerWorld/ParamLoader.h>

#include <cocos2d.h>

// extern "C"
// {
//     #include <lua.h>
//     #include <lualib.h>
//     #include <lauxlib.h>
// }

// #include <luabind/luabind.hpp>


extern DBGLayer *_g_dbg_layer;

#include "FieldPlayer.h"
#include "SoccerBall.h"
#include "SteeringBehaviors.h"
#include "Common/FSM/StateMachine.h"

// #include "Common/thirdparty/lua-5/LuaHelperFunctions.h"

USING_NS_CC;
namespace NSSoccerWorld
{

SoccerWorld::SoccerWorld(int cx, int cy) : cocos2d::Scene()
{
    _m_dbg_draw = nullptr;
    // _m_layer_action = nullptr;
    ParamLoader* p = ParamLoader::Instance();
    // _m_soccer_pitch = new SoccerPitch(cx, cy);
    _m_soccer_pitch = SoccerPitch::create(cx, cy);
    this->addChild(_m_soccer_pitch, 1);
}

SoccerWorld::~SoccerWorld()
{
}

// on "init" you need to initialize your instance
bool SoccerWorld::init()
{
    if(!cocos2d::Scene::init()) return false;

    //create a lua state
    // _m_lua = luaL_newstate();

    //open the lua libaries - new in lua5.3
    // luaL_openlibs(_m_lua);

    //open luabind
    // luabind::open(_m_lua);

    // register entity with lua
    // luabind::module(_m_lua)
    // [
    //     luabind::class_<FieldPlayer >("FieldPlayer")
    //         .def("BallWithinKickingRange", &FieldPlayer::BallWithinKickingRange)
    //         .def("isClosestTeamMemberToBall", &FieldPlayer::isClosestTeamMemberToBall)
    //         .def("Ball", &FieldPlayer::Ball)
    //         .def("Steering", &FieldPlayer::Steering)
    //         .def("GetFSM", &FieldPlayer::GetFSM),

    //     luabind::class_<SoccerBall >("SoccerBall")
    //         .def("Pos", &FieldPlayer::Pos),


    //     luabind::class_<SteeringBehaviors >("SteeringBehaviors")
    //         .def("SeekOn", &SteeringBehaviors::SeekOn)
    //         .def("SeekOff", &SteeringBehaviors::SeekOff)
    //         .def("SetTarget", &SteeringBehaviors::SetTarget),

    //     luabind::class_<StateMachine<FieldPlayer> >("StateMachine")
    //         .def("ChangeState", &StateMachine<FieldPlayer>::ChangeState),

    //     // luabind::class_<State<FieldPlayer> >("State"),

    //     luabind::class_<KickBall, bases<State<FieldPlayer> > >("KickBall")
    //         .def("Instance", &KickBall<FieldPlayer>::Instance),

    //     luabind::class_<ReturnToHomeRegion >("ReturnToHomeRegion")
    //         .def("Instance", &ReturnToHomeRegion<FieldPlayer>::Instance)
    // ];

    //load and run the script
    // RunLuaScript(_m_lua, cocos2d::FileUtils::getInstance()->fullPathForFilename("scripts/SoccerWorld/StateMachine.lua").data());
    // std::string l_script = cocos2d::FileUtils::getInstance()->fullPathForFilename("Resources/scripts/SoccerWorld/StateMachine.lua");


    // _m_lb_states = luabind::globals(_m_lua);

    _m_dbg_draw = cocos2d::DrawNode::create();
    this->addChild(_m_dbg_draw, 1000);

    scheduleUpdate();
    // schedule(CC_SCHEDULE_SELECTOR(SteeringWorld::update), 3.f);

    __LOGD("");
    return true;
}

// extern unsigned char cc_fps_images_png[];
// extern unsigned int cc_fps_images_len(void);

void SoccerWorld::onEnter()
{
    cocos2d::Scene::onEnter();

}


void SoccerWorld::update(float _a_dt)
{

    cocos2d::Scene::update(_a_dt);
    _m_soccer_pitch->update(_a_dt);

    // *_g_dbg_layer << std::to_string(_a_dt);
}

}

cocos2d::Scene *getScene()
{
    return NSSoccerWorld::SoccerWorld::createScene();
}
