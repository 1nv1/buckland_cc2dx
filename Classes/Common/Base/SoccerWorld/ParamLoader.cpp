#include "ParamLoader.h"

NSSoccerWorld::ParamLoader* NSSoccerWorld::ParamLoader::Instance()
{
  static ParamLoader instance;

  return &instance;
}