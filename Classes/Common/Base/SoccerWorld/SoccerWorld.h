#ifndef __SOCCER_WORLD_H__
#define __SOCCER_WORLD_H__

#include "cocos2d.h"

#include <Common/Base/SoccerWorld/SoccerPitch.h>

// extern "C"
// {
//     #include <lua.h>
//     #include <lualib.h>
//     #include <lauxlib.h>
// }

// #include <luabind/luabind.hpp>

namespace NSSoccerWorld
{

class SoccerWorld : public cocos2d::Scene
{
public:
    SoccerWorld(int cx, int cy);
    virtual ~SoccerWorld();

    // static cocos2d::Scene* createScene(float _a_sec_interval = 0.1f);

    virtual bool init() override;

    virtual void update(float _a_dt) override;

    virtual void onEnter() override;

protected:

public:
  // from cc2dx
  static inline cocos2d::Scene* createScene()
  {
    return SoccerWorld::create(static_cast<int>(SCREEN_X(1.f)), static_cast<int>(SCREEN_Y(1.f)));
  }

  // CREATE_FUNC(SoccerWorld);
  static inline SoccerWorld *create(int cx, int cy)
    {
        SoccerWorld *ret = new (std::nothrow) SoccerWorld(cx, cy);
        if (ret && ret->init())
        {
            ret->autorelease();
            return ret;
        }
        else
        {
            CC_SAFE_DELETE(ret);
            return nullptr;
        }
    }

    // virtual bool init() override;

    cocos2d::DrawNode *_m_dbg_draw;
    cocos2d::Layer *_m_layer_action;
    SoccerPitch *_m_soccer_pitch;

    // lua_State *_m_lua;
    // static luabind::object _m_lb_states;
};

}
#endif // __SOCCER_WORLD_H__
