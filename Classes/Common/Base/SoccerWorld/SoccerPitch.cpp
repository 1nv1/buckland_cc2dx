#include "SoccerPitch.h"
#include "SoccerBall.h"
#include "Goal.h"
#include "Common/Base/Region.h"
#include "Common/2D/Transformations.h"
#include "Common/2D/Geometry.h"
#include "SoccerTeam.h"
// #include "Debug/DebugConsole.h"
#include "Common/Base/EntityManager.h"
#include "ParamLoader.h"
#include "PlayerBase.h"
#include "TeamStates.h"
#include "Common/Misc/FrameCounter.h"

#include <Common/Misc/utils/Utils>

const int NumRegionsHorizontal = 6;
const int NumRegionsVertical   = 3;

namespace NSSoccerWorld
{
//------------------------------- ctor -----------------------------------
//------------------------------------------------------------------------
SoccerPitch::SoccerPitch(int cx, int cy):m_cxClient(cx),
                                         m_cyClient(cy),
                                         m_bPaused(false),
                                         m_bGoalKeeperHasBall(false),
                                         m_Regions(NumRegionsHorizontal*NumRegionsVertical),
                                         m_bGameOn(true)
{

  _m_layer_action = cocos2d::Layer::create();
  this->addChild(_m_layer_action);

  //define the playing area
  m_pPlayingArea = new Region(20, 20, cx-20, cy-20);

  //create the regions
  CreateRegions(PlayingArea()->Width() / (double)NumRegionsHorizontal,
                PlayingArea()->Height() / (double)NumRegionsVertical);

  //create the goals
   m_pRedGoal  = new Goal(Vector2D( m_pPlayingArea->Left(), (cy-NSSoccerWorld_Prm.GoalWidth)/2),
                          Vector2D(m_pPlayingArea->Left(), cy - (cy-NSSoccerWorld_Prm.GoalWidth)/2),
                          Vector2D(1,0));



  m_pBlueGoal = new Goal( Vector2D( m_pPlayingArea->Right(), (cy-NSSoccerWorld_Prm.GoalWidth)/2),
                          Vector2D(m_pPlayingArea->Right(), cy - (cy-NSSoccerWorld_Prm.GoalWidth)/2),
                          Vector2D(-1,0));


  //create the soccer ball
  m_pBall = SoccerBall::create(Vector2D((double)m_cxClient/2.0, (double)m_cyClient/2.0),
                           NSSoccerWorld_Prm.BallSize,
                           NSSoccerWorld_Prm.BallMass,
                           m_vecWalls,
                           "res/icons/action/ic_stars.png");
  _m_layer_action->addChild(m_pBall , 1);
  // m_pBall->drawBoundingBox();
  m_pBall->setScale(0.2f);
  __LOGD("%.2f:%.2f", m_pBall->getAnchorPoint().x, m_pBall->getAnchorPoint().y);
  // m_pBall->drawBoundingBox(cocos2d::Color4F(HEX_TO_C3B(GGC_ORANGE_300)));

  //create the teams
  m_pRedTeam  = new SoccerTeam(m_pRedGoal, m_pBlueGoal, this, SoccerTeam::team_color::red);
  this->addChild(m_pRedTeam);
  m_pRedTeam->autorelease();
  m_pRedTeam->setName("Red");

  m_pBlueTeam = new SoccerTeam(m_pBlueGoal, m_pRedGoal, this, SoccerTeam::team_color::blue);
  this->addChild(m_pBlueTeam);
  m_pBlueTeam->autorelease();
  m_pBlueTeam->setName("Blue");

  //make sure each team knows who their opponents are
  m_pRedTeam->SetOpponents(m_pBlueTeam);
  m_pBlueTeam->SetOpponents(m_pRedTeam);

  //create the walls
  Vector2D TopLeft(m_pPlayingArea->Left(), m_pPlayingArea->Top());
  Vector2D TopRight(m_pPlayingArea->Right(), m_pPlayingArea->Top());
  Vector2D BottomRight(m_pPlayingArea->Right(), m_pPlayingArea->Bottom());
  Vector2D BottomLeft(m_pPlayingArea->Left(), m_pPlayingArea->Bottom());

  m_vecWalls.push_back(Wall2D(BottomLeft, m_pRedGoal->RightPost()));
  m_vecWalls.push_back(Wall2D(m_pRedGoal->LeftPost(), TopLeft));
  m_vecWalls.push_back(Wall2D(TopLeft, TopRight));
  m_vecWalls.push_back(Wall2D(TopRight, m_pBlueGoal->LeftPost()));
  m_vecWalls.push_back(Wall2D(m_pBlueGoal->RightPost(), BottomRight));
  m_vecWalls.push_back(Wall2D(BottomRight, BottomLeft));

  // NSSoccerWorld::ParamLoader* p = NSSoccerWorld::ParamLoader::Instance();
}

//-------------------------------- dtor ----------------------------------
//------------------------------------------------------------------------
SoccerPitch::~SoccerPitch()
{
  // delete m_pBall;

  // delete m_pRedTeam;
  // delete m_pBlueTeam;

  delete m_pRedGoal;
  delete m_pBlueGoal;

  delete m_pPlayingArea;

  for (unsigned int i=0; i<m_Regions.size(); ++i)
  {
    delete m_Regions[i];
  }
}

//----------------------------- Update -----------------------------------
//
//  this demo works on a fixed frame rate (60 by default) so we don't need
//  to pass a time_elapsed as a parameter to the game entities
//------------------------------------------------------------------------
void SoccerPitch::update(float _a_dt)
{
  if (m_bPaused) return;

  static int tick = 0;

  //update the balls
  m_pBall->update(_a_dt);

  //update the teams
  m_pRedTeam->update(_a_dt);
  m_pBlueTeam->update(_a_dt);

  //if a goal has been detected reset the pitch ready for kickoff
  if (m_pBlueGoal->Scored(m_pBall) || m_pRedGoal->Scored(m_pBall))
  {
    m_bGameOn = false;

    //reset the ball
    m_pBall->PlaceAtPosition(Vector2D((double)m_cxClient/2.0, (double)m_cyClient/2.0));

    //get the teams ready for kickoff
    m_pRedTeam->GetFSM()->ChangeState(PrepareForKickOff::Instance());
    m_pBlueTeam->GetFSM()->ChangeState(PrepareForKickOff::Instance());
  }

}

//------------------------- CreateRegions --------------------------------
void SoccerPitch::CreateRegions(double width, double height)
{
  //index into the vector
  int idx = m_Regions.size()-1;

  for (int col=0; col<NumRegionsHorizontal; ++col)
  {
    for (int row=0; row<NumRegionsVertical; ++row)
    {
      m_Regions[idx--] = new Region(PlayingArea()->Left()+col*width,
                                   PlayingArea()->Top()+row*height,
                                   PlayingArea()->Left()+(col+1)*width,
                                   PlayingArea()->Top()+(row+1)*height,
                                   idx);
    }
  }
}


//------------------------------ Render ----------------------------------
//------------------------------------------------------------------------
bool SoccerPitch::Render()
{
  // //draw the grass
  // gdi->DarkGreenPen();
  // gdi->DarkGreenBrush();
  // gdi->Rect(0,0,m_cxClient, m_cyClient);

  // //render regions
  // if (NSSoccerWorld_Prm.bRegions)
  // {
  //   for (unsigned int r=0; r<m_Regions.size(); ++r)
  //   {
  //     m_Regions[r]->Render(true);
  //   }
  // }

  // //render the goals
  // gdi->HollowBrush();
  // gdi->RedPen();
  // gdi->Rect(m_pPlayingArea->Left(), (m_cyClient-NSSoccerWorld_Prm.GoalWidth)/2, m_pPlayingArea->Left()+40, m_cyClient - (m_cyClient-NSSoccerWorld_Prm.GoalWidth)/2);

  // gdi->BluePen();
  // gdi->Rect(m_pPlayingArea->Right(), (m_cyClient-NSSoccerWorld_Prm.GoalWidth)/2, m_pPlayingArea->Right()-40, m_cyClient - (m_cyClient-NSSoccerWorld_Prm.GoalWidth)/2);

  // //render the pitch markings
  // gdi->WhitePen();
  // gdi->Circle(m_pPlayingArea->Center(), m_pPlayingArea->Width() * 0.125);
  // gdi->Line(m_pPlayingArea->Center().x, m_pPlayingArea->Top(), m_pPlayingArea->Center().x, m_pPlayingArea->Bottom());
  // gdi->WhiteBrush();
  // gdi->Circle(m_pPlayingArea->Center(), 2.0);


  // //the ball
  // gdi->WhitePen();
  // gdi->WhiteBrush();
  // m_pBall->Render();

  // //Render the teams
  // m_pRedTeam->Render();
  // m_pBlueTeam->Render();

  // //render the walls
  // gdi->WhitePen();
  // for (unsigned int w=0; w<m_vecWalls.size(); ++w)
  // {
  //   m_vecWalls[w].Render();
  // }

  // //show the score
  // gdi->TextColor(Cgdi::red);
  // gdi->TextAtPos((m_cxClient/2)-50, m_cyClient-18, "Red: " + ttos(m_pBlueGoal->NumGoalsScored()));

  // gdi->TextColor(Cgdi::blue);
  // gdi->TextAtPos((m_cxClient/2)+10, m_cyClient-18, "Blue: " + ttos(m_pRedGoal->NumGoalsScored()));

  return true;
}

// from cc2dx
bool SoccerPitch::init()
{
  if(!cocos2d::Layer::init()) return false;

  cocos2d::LayerColor *_l_background = cocos2d::LayerColor::create(cocos2d::Color4B(HEX_TO_C3B(GGC_GREEN_900)), SCREEN_X(1.f),SCREEN_Y(1.f));
    this->addChild(_l_background, -1);

  // _m_dn_pitch = cocos2d::DrawNode::create();
  // _l_background->addChild(_m_dn_pitch);

  // // draw m_pPlayingArea
  // cocos2d::Vec2 _l_left_top = cocos2d::Vec2(m_pPlayingArea->Left(), m_pPlayingArea->Bottom());
  // cocos2d::Vec2 _l_right_bottom = cocos2d::Vec2(m_pPlayingArea->Right(), m_pPlayingArea->Top());

  // _m_dn_pitch->drawRect(_l_left_top, _l_right_bottom, cocos2d::Color4F::WHITE);

  // // draw center : line + circle
  // _m_dn_pitch->drawLine(cocos2d::Vec2(m_pPlayingArea->Center().x, m_pPlayingArea->Bottom()), cocos2d::Vec2(m_pPlayingArea->Center().x, m_pPlayingArea->Top()), cocos2d::Color4F::WHITE);

  // _m_dn_pitch->drawCircle(VectorToccVec2(m_pPlayingArea->Center()), m_pPlayingArea->Width() * 0.125, 360, 32, false, cocos2d::Color4F::WHITE);

  // // draw goals
  // _m_dn_pitch->drawRect(cocos2d::Vec2(m_pPlayingArea->Left(), (m_cyClient-NSSoccerWorld_Prm.GoalWidth)/2), cocos2d::Vec2(m_pPlayingArea->Left()+m_pPlayingArea->Width() * 0.125, m_cyClient - (m_cyClient-NSSoccerWorld_Prm.GoalWidth)/2), cocos2d::Color4F::WHITE);

  // _m_dn_pitch->drawRect(cocos2d::Vec2(m_pPlayingArea->Right(), (m_cyClient-NSSoccerWorld_Prm.GoalWidth)/2), cocos2d::Vec2(m_pPlayingArea->Right()-m_pPlayingArea->Width() * 0.125, m_cyClient - (m_cyClient-NSSoccerWorld_Prm.GoalWidth)/2), cocos2d::Color4F::WHITE);

  return true;
}


}





