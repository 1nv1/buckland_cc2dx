#include "FieldPlayer.h"
#include "PlayerBase.h"
#include "SteeringBehaviors.h"
#include "Common/2D/Transformations.h"
#include "Common/2D/Geometry.h"
// #include "Common/Misc/Cgdi.h"
#include "Common/2D/C2DMatrix.h"
#include "Goal.h"
#include "Common/Base/Region.h"
#include "Common/Base/EntityFunctionTemplates.h"
#include "ParamLoader.h"
#include "SoccerTeam.h"
#include "Common/Time/Regulator.h"
// #include "Common/Debug/DebugConsole.h"


#include <limits>

using std::vector;

namespace NSSoccerWorld
{
//------------------------------- dtor ---------------------------------------
//----------------------------------------------------------------------------
FieldPlayer::~FieldPlayer()
{
  delete m_pKickLimiter;
  delete m_pStateMachine;
}

//----------------------------- ctor -------------------------------------
//------------------------------------------------------------------------
FieldPlayer::FieldPlayer(SoccerTeam* home_team,
                      int   home_region,
                      State<FieldPlayer>* start_state,
                      Vector2D  heading,
                      Vector2D velocity,
                      double    mass,
                      double    max_force,
                      double    max_speed,
                      double    max_turn_rate,
                      double    scale,
                      player_role role): PlayerBase(home_team,
                                                    home_region,
                                                    heading,
                                                    velocity,
                                                    mass,
                                                    max_force,
                                                    max_speed,
                                                    max_turn_rate,
                                                    scale,
                                                    role)
{
  // //set up the state machine
  // m_pStateMachine =  new StateMachine<FieldPlayer>(this);

  // if (start_state)
  // {
  //   m_pStateMachine->SetCurrentState(start_state);
  //   m_pStateMachine->SetPreviousState(start_state);
  //   m_pStateMachine->SetGlobalState(GlobalPlayerState::Instance());

  //   m_pStateMachine->CurrentState()->Enter(this);
  // }

  // m_pSteering->SeparationOn();

  // //set up the kick regulator
  // m_pKickLimiter = new Regulator(NSSoccerWorld_Prm.PlayerKickFrequency);
}

//------------------------------ Update ----------------------------------
//
//
//------------------------------------------------------------------------
void FieldPlayer::update(float _a_dt)
{
  //run the logic for the current state
  m_pStateMachine->Update();

  //calculate the combined steering force
  m_pSteering->Calculate();

  //if no steering force is produced decelerate the player by applying a
  //braking force
  if (m_pSteering->Force().isZero())
  {
    const double BrakingRate = 0.8;

    m_vVelocity = m_vVelocity * BrakingRate;
  }

  //the steering force's side component is a force that rotates the
  //player about its axis. We must limit the rotation so that a player
  //can only turn by PlayerMaxTurnRate rads per update.
  double TurningForce =   m_pSteering->SideComponent();

  Clamp(TurningForce, -NSSoccerWorld_Prm.PlayerMaxTurnRate, NSSoccerWorld_Prm.PlayerMaxTurnRate);

  //rotate the heading vector
  Vec2DRotateAroundOrigin(m_vHeading, TurningForce);

  //make sure the velocity vector points in the same direction as
  //the heading vector
  m_vVelocity = m_vHeading * m_vVelocity.length();

  //and recreate m_vSide
  m_vSide = m_vHeading.getPerp();


  //now to calculate the acceleration due to the force exerted by
  //the forward component of the steering force in the direction
  //of the player's heading
  Vector2D accel = m_vHeading * m_pSteering->ForwardComponent() / m_dMass;

  m_vVelocity += accel;

  //make sure player does not exceed maximum velocity
  truncate(&m_vVelocity, m_dMaxSpeed);

  //update the position
  // m_vPosition += m_vVelocity;
  cocos2d::Node::setPosition(_position + m_vVelocity);


  //enforce a non-penetration constraint if desired
  if(NSSoccerWorld_Prm.bNonPenetrationConstraint)
  {
    EnforceNonPenetrationConstraint(this, AutoList<PlayerBase>::GetAllMembers());
  }

  cocos2d::Vec2 _l_temp = m_vHeading;
  float _l_angle = CC_RADIANS_TO_DEGREES(atan2f(_l_temp.x, _l_temp.y));
  this->setRotation(_l_angle);
}

//-------------------- HandleMessage -------------------------------------
//
//  routes any messages appropriately
//------------------------------------------------------------------------
bool FieldPlayer::HandleMessage(const Telegram& msg)
{
  return m_pStateMachine->HandleMessage(msg);
}

//--------------------------- Render -------------------------------------
//
//------------------------------------------------------------------------
void FieldPlayer::Render()
{
  // gdi->TransparentText();
  // gdi->TextColor(Cgdi::grey);

  // //set appropriate team color
  // if (Team()->Color() == SoccerTeam::blue){gdi->BluePen();}
  // else{gdi->RedPen();}



  // //render the player's body
  // m_vecPlayerVBTrans = WorldTransform(m_vecPlayerVB,
  //                                        Pos(),
  //                                        Heading(),
  //                                        Side(),
  //                                        Scale());
  // gdi->ClosedShape(m_vecPlayerVBTrans);

  // //and 'is 'ead
  // gdi->BrownBrush();
  // if (NSSoccerWorld_Prm.bHighlightIfThreatened && (Team()->ControllingPlayer() == this) && isThreatened()) gdi->YellowBrush();
  // gdi->Circle(Pos(), 6);


  // //render the state
  // if (NSSoccerWorld_Prm.bStates)
  // {
  //   gdi->TextColor(0, 170, 0);
  //   gdi->TextAtPos(m_vPosition.x, m_vPosition.y -20, std::string(m_pStateMachine->GetNameOfCurrentState()));
  // }

  // //show IDs
  // if (NSSoccerWorld_Prm.bIDs)
  // {
  //   gdi->TextColor(0, 170, 0);
  //   gdi->TextAtPos(Pos().x-20, Pos().y-20, ttos(ID()));
  // }


  // if (NSSoccerWorld_Prm.bViewTargets)
  // {
  //   gdi->RedBrush();
  //   gdi->Circle(Steering()->Target(), 3);
  //   gdi->TextAtPos(Steering()->Target(), ttos(ID()));
  // }
}

// from cc2dx
FieldPlayer *FieldPlayer::create(SoccerTeam*        home_team,
              int                home_region,
              State<FieldPlayer>* start_state,
              Vector2D           heading,
              Vector2D           velocity,
              double              mass,
              double              max_force,
              double              max_speed,
              double              max_turn_rate,
              double              scale,
              player_role         role,
                                  const std::string &_a_file)
{
  // cocos2d::Sprite::create(_a_file);
    // Sprite *sprite = new (std::nothrow) Sprite();
  FieldPlayer *_l_self = new (std::nothrow) FieldPlayer( home_team,
                                                              home_region,
                                                              start_state,
                                                              heading,
                                                              velocity,
                                                              mass,
                                                              max_force,
                                                              max_speed,
                                                              max_turn_rate,
                                                              scale,
                                                              role);
    if (_l_self && _a_file != "" && _l_self->init())
    {
        _l_self->autorelease();
        _l_self->setSprite(cocos2d::Sprite::create(_a_file));
        //set up the state machine
        _l_self->m_pStateMachine =  new StateMachine<FieldPlayer>(_l_self);

        if (start_state)
        {
          _l_self->m_pStateMachine->SetCurrentState(start_state);
          _l_self->m_pStateMachine->SetPreviousState(start_state);
          _l_self->m_pStateMachine->SetGlobalState(GlobalPlayerState::Instance());

          _l_self->m_pStateMachine->CurrentState()->Enter(_l_self);
        }

        _l_self->m_pSteering->SeparationOn();

        //set up the kick regulator
        _l_self->m_pKickLimiter = new Regulator(NSSoccerWorld_Prm.PlayerKickFrequency);
        return _l_self;
    }
    CC_SAFE_DELETE(_l_self);
    return nullptr;

}

}

