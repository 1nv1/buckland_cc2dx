#include "GoalKeeper.h"
// #include "Misc/Cgdi.h"
#include "SteeringBehaviors.h"
#include "SoccerTeam.h"
#include "SoccerPitch.h"
#include "Common/2D/Transformations.h"
#include "GoalKeeperStates.h"
#include "Goal.h"
#include "Common/Base/EntityFunctionTemplates.h"
#include "ParamLoader.h"


namespace NSSoccerWorld
{
//----------------------------- ctor ------------------------------------
//-----------------------------------------------------------------------
GoalKeeper::GoalKeeper(SoccerTeam*        home_team,
                       int                home_region,
                       State<GoalKeeper>* start_state,
                       Vector2D           heading,
                       Vector2D           velocity,
                       double              mass,
                       double              max_force,
                       double              max_speed,
                       double              max_turn_rate,
                       double              scale): PlayerBase(home_team,
                                                             home_region,
                                                             heading,
                                                             velocity,
                                                             mass,
                                                             max_force,
                                                             max_speed,
                                                             max_turn_rate,
                                                             scale,
                                                             PlayerBase::goal_keeper)


{
  //  //set up the state machine
  // m_pStateMachine = new StateMachine<GoalKeeper>(this);

  // m_pStateMachine->SetCurrentState(start_state);
  // m_pStateMachine->SetPreviousState(start_state);
  // m_pStateMachine->SetGlobalState(GlobalKeeperState::Instance());

  // m_pStateMachine->CurrentState()->Enter(this);
}



//-------------------------- Update --------------------------------------

void GoalKeeper::update(float _a_dt)
{
  //run the logic for the current state
  m_pStateMachine->Update();

  //calculate the combined force from each steering behavior
  Vector2D SteeringForce = m_pSteering->Calculate();



  //Acceleration = Force/Mass
  Vector2D Acceleration = SteeringForce / m_dMass;

  //update velocity
  m_vVelocity += Acceleration;

  //make sure player does not exceed maximum velocity
  truncate(&m_vVelocity, m_dMaxSpeed);

  //update the position
  // m_vPosition += m_vVelocity;
  cocos2d::Node::setPosition(_position + m_vVelocity);

  //enforce a non-penetration constraint if desired
  if(NSSoccerWorld_Prm.bNonPenetrationConstraint)
  {
    EnforceNonPenetrationConstraint(this, AutoList<PlayerBase>::GetAllMembers());
  }

  //update the heading if the player has a non zero velocity
  if ( !m_vVelocity.isZero())
  {
    m_vHeading = m_vVelocity.getNormalized();

    m_vSide = m_vHeading.getPerp();
  }

  //look-at vector always points toward the ball
  if (!Pitch()->GoalKeeperHasBall())
  {
   m_vLookAt = (Ball()->Pos() - Pos()).getNormalized();
  }

  cocos2d::Vec2 _l_temp = m_vHeading;
  float _l_angle = CC_RADIANS_TO_DEGREES(atan2f(_l_temp.x, _l_temp.y));
  this->setRotation(_l_angle);
}


bool GoalKeeper::BallWithinRangeForIntercept()const
{
  return (length_sq(Team()->HomeGoal()->Center() - Ball()->Pos()) <=
          NSSoccerWorld_Prm.GoalKeeperInterceptRangeSq);
}

bool GoalKeeper::TooFarFromGoalMouth()const
{
  return (length_sq(Pos() - GetRearInterposeTarget()) >
          NSSoccerWorld_Prm.GoalKeeperInterceptRangeSq);
}

Vector2D GoalKeeper::GetRearInterposeTarget()const
{
  double xPosTarget = Team()->HomeGoal()->Center().x;

  double yPosTarget = Pitch()->PlayingArea()->Center().y -
                     NSSoccerWorld_Prm.GoalWidth*0.5 + (Ball()->Pos().y*NSSoccerWorld_Prm.GoalWidth) /
                     Pitch()->PlayingArea()->Height();

  return Vector2D(xPosTarget, yPosTarget);
}

//-------------------- HandleMessage -------------------------------------
//
//  routes any messages appropriately
//------------------------------------------------------------------------
bool GoalKeeper::HandleMessage(const Telegram& msg)
{
  return m_pStateMachine->HandleMessage(msg);
}

//--------------------------- Render -------------------------------------
//
//------------------------------------------------------------------------
void GoalKeeper::Render()
{
  // if (Team()->Color() == SoccerTeam::blue)
  //   gdi->BluePen();
  // else
  //   gdi->RedPen();

  // m_vecPlayerVBTrans = WorldTransform(m_vecPlayerVB,
  //                                      Pos(),
  //                                      m_vLookAt,
  //                                      m_vLookAt.Perp(),
  //                                      Scale());

  // gdi->ClosedShape(m_vecPlayerVBTrans);

  // //draw the head
  // gdi->BrownBrush();
  // gdi->Circle(Pos(), 6);

  // //draw the ID
  // if (NSSoccerWorld_Prm.bIDs)
  // {
  //   gdi->TextColor(0, 170, 0);;
  //   gdi->TextAtPos(Pos().x-20, Pos().y-20, ttos(ID()));
  // }

  // //draw the state
  // if (NSSoccerWorld_Prm.bStates)
  // {
  //   gdi->TextColor(0, 170, 0);
  //   gdi->TransparentText();
  //   gdi->TextAtPos(m_vPosition.x, m_vPosition.y -20, std::string(m_pStateMachine->GetNameOfCurrentState()));
  // }
}

// from cc2dx
GoalKeeper *GoalKeeper::create(SoccerTeam*        home_team,
              int                home_region,
              State<GoalKeeper>* start_state,
              Vector2D           heading,
              Vector2D           velocity,
              double              mass,
              double              max_force,
              double              max_speed,
              double              max_turn_rate,
              double              scale,
                                  const std::string &_a_file)
{
  GoalKeeper *_l_self = new (std::nothrow) GoalKeeper( home_team,
                                                              home_region,
                                                              start_state,
                                                              heading,
                                                              velocity,
                                                              mass,
                                                              max_force,
                                                              max_speed,
                                                              max_turn_rate,
                                                              scale);
    if (_l_self && _a_file != "" && _l_self->init())
    {
        _l_self->autorelease();
        _l_self->setSprite(cocos2d::Sprite::create(_a_file));
        //set up the state machine
        _l_self->m_pStateMachine = new StateMachine<GoalKeeper>(_l_self);

        _l_self->m_pStateMachine->SetCurrentState(start_state);
        _l_self->m_pStateMachine->SetPreviousState(start_state);
        _l_self->m_pStateMachine->SetGlobalState(GlobalKeeperState::Instance());
        _l_self->m_pStateMachine->CurrentState()->Enter(_l_self);

        return _l_self;
    }
    CC_SAFE_DELETE(_l_self);
    return nullptr;

}

}