#include <cocos2d.h>

#include "ScriptingWorld.h"

#include <Common/Debug/DBGLayer.h>

#include <Common/Misc/utils/Utils>


#include <sol.hpp>

#include <string>
#include <iostream>
// using namespace std;

//include the luabind headers. Make sure you have the paths set correctly
//to the lua, luabind and Boost files.
// #include <luabind/luabind.hpp>
// using namespace luabind;

// #include "Common/thirdparty/lua-5/LuaHelperFunctions.h"
#include "Entity.h"
#include "Miner.h"
#include "ScriptedStateMachine.h"

USING_NS_CC;

extern DBGLayer *_g_dbg_layer;

#define TRY(prog) \
try { prog; } \
catch (std::exception const &e) { __LOGE("%s", e.what()); }

namespace NSScriptingWorld
{

// void RegisterScriptedStateMachineWithLua(lua_State* pLua)
// {
//     luabind::module(pLua)
//     [
//         luabind::class_<ScriptedStateMachine<Miner> >("ScriptedStateMachine")
//             .def("ChangeState", &ScriptedStateMachine<Miner>::ChangeState)
//             .def("CurrentState", &ScriptedStateMachine<Miner>::CurrentState)
//             .def("SetCurrentState", &ScriptedStateMachine<Miner>::SetCurrentState)
//     ];
// }


// void RegisterEntityWithLua(lua_State* pLua)
// {
//     luabind::module(pLua)
//     [
//         luabind::class_<Entity>("Entity")
//             .def("Name", &Entity::Name)
//             .def("ID", &Entity::ID)
//     ];
// }


// void RegisterMinerWithLua(lua_State* pLua)
// {
//     luabind::module(pLua)
//     [
//         luabind::class_<Miner, luabind::bases<Entity> >("Miner")
//             .def("GoldCarried", &Miner::GoldCarried)
//             .def("SetGoldCarried", &Miner::SetGoldCarried)
//             .def("AddToGoldCarried", &Miner::AddToGoldCarried)
//             .def("Fatigued", &Miner::Fatigued)
//             .def("DecreaseFatigue", &Miner::DecreaseFatigue)
//             .def("IncreaseFatigue", &Miner::IncreaseFatigue)
//             .def("GetFSM", &Miner::GetFSM)
//     ];
// }


ScriptingWorld::ScriptingWorld(float _a_sec_interval) : cocos2d::Scene()
{
    _m_sec_interval = _a_sec_interval;
}

ScriptingWorld::~ScriptingWorld()
{
    __LOGD("");
    // lua_close(_m_lua);
}

// on "init" you need to initialize your instance
bool ScriptingWorld::init()
{
    if(!cocos2d::Scene::init()) return false;

    *_g_dbg_layer << "hello world";

    //create a lua state
    // _m_lua = luaL_newstate();
    m_sol_state.open_libraries( sol::lib::base, sol::lib::jit, sol::lib::ffi, sol::lib::io, sol::lib::os, sol::lib::debug);

    // LuaExceptionGuard guard(_m_lua);

    //open the lua libaries - new in lua5.1
    // luaL_openlibs(_m_lua);

    //open luabind
    // luabind::open(_m_lua);

    //bind the relevant classes to Lua
    // RegisterEntityWithLua(_m_lua);
    m_sol_state.new_usertype<Entity>( "Entity"
        // , sol::constructors<Entity()>()
        // , "hp", sol::property(&Entity::get_hp, &Entity::set_hp)
        , "Name", &Entity::Name
        , "ID", &Entity::ID
        );

    // RegisterMinerWithLua(_m_lua);
    m_sol_state.new_usertype<Miner>( "Miner"
        // , sol::constructors<Miner(std::string)>()
        , sol::base_classes, sol::bases<Entity>()
            ,"GoldCarried", &Miner::GoldCarried
            ,"SetGoldCarried", &Miner::SetGoldCarried
            ,"AddToGoldCarried", &Miner::AddToGoldCarried
            ,"Fatigued", &Miner::Fatigued
            ,"DecreaseFatigue", &Miner::DecreaseFatigue
            ,"IncreaseFatigue", &Miner::IncreaseFatigue
            ,"GetFSM", &Miner::GetFSM
        );

    // RegisterScriptedStateMachineWithLua(_m_lua);
    m_sol_state.new_usertype<ScriptedStateMachine<Miner>>( "ScriptedStateMachine"
        // , sol::constructors<ScriptedStateMachine<Miner>(Miner *)>()
            ,"ChangeState", &ScriptedStateMachine<Miner>::ChangeState
            ,"CurrentState", &ScriptedStateMachine<Miner>::CurrentState
            ,"SetCurrentState", &ScriptedStateMachine<Miner>::SetCurrentState
        );

    m_sol_state["cclog"] = &cocos2d::log;

    //load and run the script
    // RunLuaScript(_m_lua, cocos2d::FileUtils::getInstance()->fullPathForFilename("scripts/ScriptingWorld/StateMachineScript.lua").data());
    std::string l_script = cocos2d::FileUtils::getInstance()->fullPathForFilename("scripts/ScriptingWorld/StateMachineScript.lua");
    __LOGD("%s", l_script.data());
    TRY(m_sol_state.script_file(l_script));

    //create a miner
    _m_bob = new Miner("bob");

    //grab the global table from the lua state. This will inlclude
    //all the functions and variables defined in the scripts run so far
    //(StateMachineScript.lua in this example)
    // static luabind::object states = luabind::globals(_m_lua);

    //ensure states is a table
    // if (luabind::type(states) == LUA_TTABLE)
    // {
    //     //make sure Bob's CurrentState object is set to a valid state.
    //     _m_bob->GetFSM()->SetCurrentState(states["State"]["GoHome"]);
        _m_bob->GetFSM()->SetCurrentState(m_sol_state["State"]["GoHome"]);
    //     //run him through a few update cycles
    //     // _m_bob->Update();
        schedule(CC_SCHEDULE_SELECTOR(ScriptingWorld::update), _m_sec_interval);
    // }

    return true;
}

void ScriptingWorld::onEnter()
{
    cocos2d::Scene::onEnter();

    __LOGD("0x%lx", DIRECTOR->getTextureCache()->getTextureForKey("/my_img"));

}


void ScriptingWorld::update(float _a_dt)
{
    _m_bob->Update();
    // __LOGD("");
    // lua_close(_m_lua);Z
}

}

cocos2d::Scene *getScene()
{
    return NSScriptingWorld::ScriptingWorld::createScene(0.25f);
}
