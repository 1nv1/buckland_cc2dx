#ifndef SCRIPTEDScriptedStateMachine_H
#define SCRIPTEDScriptedStateMachine_H
// #pragma warning (disable : 4786)
//------------------------------------------------------------------------
//
//  Name:   ScriptedStateMachine.h
//
//  Desc:   A simple scripted state machine class. Inherit from this class and
//          create some states in Lua to give your agents FSM functionality
//
//  Author: Mat Buckland 2003 (fup@ai-junkie.com)
//
//------------------------------------------------------------------------

// extern "C"
// {
//     #include <lua.h>
// }

// #include <luabind/luabind.hpp>
#include <sol.hpp>
#include <cassert>

namespace NSScriptingWorld
{

template <class entity_type>
class ScriptedStateMachine
{

private:

  //pointer to the agent that owns this instance
  entity_type*      m_pOwner;

  //the current state is a lua table of lua functions. A table may be
  //represented in C++ using a luabind::object
  // luabind::object   m_CurrentState;
  sol::object m_sol_obj;

public:

  ScriptedStateMachine(entity_type* owner)
    :m_pOwner(owner)
    {}

  //use these methods to initialize the FSM
  void SetCurrentState(const sol::object &s){ m_sol_obj = s; }


  //call this to update the FSM
  void  Update()
  {
    //make sure the state is valid before calling its Execute 'method'
    if (static_cast<bool>(m_sol_obj))
    {
      // m_sol_obj["Execute"].as<std::function<void(entity_type *)>>()(m_pOwner);
      m_sol_obj.as<sol::userdata>()["Execute"](m_pOwner);
    }
  }

  //change to a new state
  void  ChangeState(const sol::object &new_state)
  {
    //call the exit method of the existing state
    m_sol_obj.as<sol::userdata>()["Exit"](m_pOwner);

    //change state to the new state
    m_sol_obj = new_state;

    //call the entry method of the new state
    m_sol_obj.as<sol::userdata>()["Enter"](m_pOwner);
  }

  //retrieve the current state
  const sol::object&  CurrentState()const{return m_sol_obj;}
};

}


#endif


