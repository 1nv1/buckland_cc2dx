#ifndef __WEST_WORLD_H__
#define __WEST_WORLD_H__

#include "cocos2d.h"

#include <sol.hpp>

#include "Miner.h"

namespace NSScriptingWorld {

class ScriptingWorld : public cocos2d::Scene
{
public:
    ScriptingWorld(float a_sec_interval);
    virtual ~ScriptingWorld();

    // static cocos2d::Scene* createScene();

    virtual bool init() override;

    virtual void update(float _a_dt) override;

    virtual void onEnter() override;

public:

  static inline ScriptingWorld *createScene(float a_sec_interval)
    {
        ScriptingWorld *ret = new (std::nothrow) ScriptingWorld(a_sec_interval);
        if (ret && ret->init())
        {
            ret->autorelease();
            return ret;
        }
        else
        {
            CC_SAFE_DELETE(ret);
            return nullptr;
        }
    }

    cocos2d::DrawNode *m_dn_dbg;
    cocos2d::Layer *_m_layer_action;

    // lua_State *_m_lua;
    sol::state m_sol_state;
    Miner *_m_bob;
    float _m_sec_interval;
};

}
#endif // __ScriptingWorld_H__
