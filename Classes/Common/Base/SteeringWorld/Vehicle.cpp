#include "Vehicle.h"
#include "Common/2D/C2DMatrix.h"
#include "Common/2D/Geometry.h"
#include "SteeringBehaviors.h"
#include "Common/2D/Transformations.h"
#include "SteeringWorld.h"
#include "Common/Misc/CellSpacePartition.h"
// #include "Common/Misc/cgdi.h"

#include <cocos2d.h>

#include <Common/Debug/DBGLayer.h>

using std::vector;
using std::list;

namespace NSSteeringWorld {

//----------------------------- ctor -------------------------------------
//------------------------------------------------------------------------
Vehicle::Vehicle(SteeringWorld* world,
               Vector2D position,
               double    rotation,
               Vector2D velocity,
               double    mass,
               double    max_force,
               double    max_speed,
               double    max_turn_rate,
               double    scale,
               int32_t   _a_flag /*= 0*/):    MovingEntity(position,
                                                 scale,
                                                 velocity,
                                                 max_speed,
                                                 Vector2D(sin(rotation),-cos(rotation)),
                                                 mass,
                                                 Vector2D(scale,scale),
                                                 max_turn_rate,
                                                 max_force),
                                      cocos2d::Node(),
                                       m_pWorld(world),
                                       m_vSmoothedHeading(Vector2D(0,0)),
                                       m_bSmoothingOn(false),
                                       m_dTimeElapsed(0.0)
{
  InitializeBuffer();

  //set up the steering behavior class
  m_pSteering = new NSSteeringWorld::SteeringBehavior(this, _a_flag);

  //set up the smoother
  m_pHeadingSmoother = new Smoother<Vector2D>(NSSteeringWorld_Prm.NumSamplesForSmoothing, Vector2D(0.0, 0.0));

}


//---------------------------- dtor -------------------------------------
//-----------------------------------------------------------------------
Vehicle::~Vehicle()
{
  delete m_pSteering;
  delete m_pHeadingSmoother;
}

//------------------------------ Update ----------------------------------
//
//  Updates the vehicle's position from a series of steering behaviors
//------------------------------------------------------------------------
void Vehicle::update(float time_elapsed)
{
  extern DBGLayer *_g_dbg_layer;

  cocos2d::Node::update(time_elapsed);
  //update the time elapsed
  m_dTimeElapsed = time_elapsed;

  //keep a record of its old position so we can update its cell later
  //in this method
  Vector2D OldPos = Pos();


  Vector2D SteeringForce;

  //calculate the combined force from each steering behavior in the
  //vehicle's list
  SteeringForce = m_pSteering->Calculate();

  //Acceleration = Force/Mass
  Vector2D acceleration = SteeringForce / m_dMass;

  //update velocity
  m_vVelocity += acceleration * time_elapsed;

  //make sure vehicle does not exceed maximum velocity
  truncate(&m_vVelocity, m_dMaxSpeed);

  //update the position
  Vector2D _l_position = _position;
  _l_position += m_vVelocity * time_elapsed;

  //update the heading if the vehicle has a non zero velocity
  if (length_sq(m_vVelocity) > 0.00000001)
  {
    m_vHeading = (m_vVelocity).getNormalized();

    m_vSide = m_vHeading.getPerp();
  }

  EnforceNonPenetrationConstraint(this, World()->Agents());

  //treat the screen as a toroid
  WrapAround(_l_position, m_pWorld->cxClient(), m_pWorld->cyClient());

  //update the vehicle's current cell if space partitioning is turned on
  if (Steering()->isSpacePartitioningOn())
  {
    World()->CellSpace()->UpdateEntity(this, OldPos);
  }

  if (isSmoothingOn())
  {
    m_vSmoothedHeading = m_pHeadingSmoother->Update(Heading());
  }

  cocos2d::Vec2 _l_temp = m_vHeading;

  // float _l_dot = cocos2d::Vec2::dot(_l_temp, cocos2d::Vec2(0,1));
  // float _l_angle = CC_RADIANS_TO_DEGREES(std::acos( _l_dot / _l_temp.length()));
  float _l_angle = CC_RADIANS_TO_DEGREES(atan2f(_l_temp.x, _l_temp.y));
  this->setRotation(_l_angle);

  // __LOGD("%d: %.2f", this->ID(), _l_angle);

  cocos2d::Node::setPosition(_l_position);
}


//-------------------------------- Render -------------------------------------
//-----------------------------------------------------------------------------
// void Vehicle::Render()
// {
//   //a vector to hold the transformed vertices
//   static std::vector<Vector2D>  m_vecVehicleVBTrans;

//   //render neighboring vehicles in different colors if requested
//   if (m_pWorld->RenderNeighbors())
//   {
//     if (ID() == 0) gdi->RedPen();
//     else if(IsTagged()) gdi->GreenPen();
//     else gdi->BluePen();
//   }

//   else
//   {
//     gdi->BluePen();
//   }

//   if (Steering()->isInterposeOn())
//   {
//     gdi->RedPen();
//   }

//   if (Steering()->isHideOn())
//   {
//     gdi->GreenPen();
//   }

//   if (isSmoothingOn())
//   {
//     m_vecVehicleVBTrans = WorldTransform(m_vecVehicleVB,
//                                          Pos(),
//                                          SmoothedHeading(),
//                                          SmoothedHeading().Perp(),
//                                          Scale());
//   }

//   else
//   {
//     m_vecVehicleVBTrans = WorldTransform(m_vecVehicleVB,
//                                          Pos(),
//                                          Heading(),
//                                          Side(),
//                                          Scale());
//   }


//   gdi->ClosedShape(m_vecVehicleVBTrans);

//   //render any visual aids / and or user options
//   if (m_pWorld->ViewKeys())
//   {
//     Steering()->RenderAids();
//   }
// }


//----------------------------- InitializeBuffer -----------------------------
//
//  fills the vehicle's shape buffer with its vertices
//-----------------------------------------------------------------------------
void Vehicle::InitializeBuffer()
{
  const int NumVehicleVerts = 3;

  Vector2D vehicle[NumVehicleVerts] = {Vector2D(-1.0f,0.6f),
                                        Vector2D(1.0f,0.0f),
                                        Vector2D(-1.0f,-0.6f)};

  //setup the vertex buffers and calculate the bounding radius
  for (int vtx=0; vtx<NumVehicleVerts; ++vtx)
  {
    m_vecVehicleVB.push_back(vehicle[vtx]);
  }
}

// from cc2dx
Vehicle *Vehicle::create(SteeringWorld* world,
                        Vector2D position,
                        double    rotation,
                        Vector2D velocity,
                        double    mass,
                        double    max_force,
                        double    max_speed,
                        double    max_turn_rate,
                        double    scale,
                        int32_t  _a_flag /*= 0*/,
                        const std::string &_a_file/*=""*/)
{
  // cocos2d::Node::create(_a_file);
    // Node *Node = new (std::nothrow) Node();
  Vehicle *_l_self = new (std::nothrow) Vehicle(world,
                        position,
                        rotation,
                        velocity,
                        mass,
                        max_force,
                        max_speed,
                        max_turn_rate,
                        scale);
    if (_l_self && _a_file != "" && _l_self->init())
    {
        _l_self->autorelease();
        _l_self->setSprite(cocos2d::Sprite::create(_a_file));
        return _l_self;
    }
    CC_SAFE_DELETE(_l_self);
    return nullptr;

}

bool Vehicle::init()
{
  if(!BaseGameEntity::init()) return false;

  m_dBoundingRadius = getBoundingBox().size.width;

  // scheduleUpdate();
  return true;
}

}