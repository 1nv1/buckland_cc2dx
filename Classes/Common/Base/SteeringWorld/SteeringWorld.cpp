#include "SteeringWorld.h"
#include "Common/Base/SteeringWorld/Vehicle.h"
#include "Common/Base/SteeringWorld/constants.h"
#include "Common/Base/SteeringWorld/Obstacle.h"
#include "Common/2D/Geometry.h"
#include "Common/2D/Wall2D.h"
#include "Common/2D/Transformations.h"
#include "Common/Base/SteeringWorld/SteeringBehaviors.h"
#include "Common/Time/PrecisionTimer.h"
#include "Common/Misc/Smoother.h"
#include "Common/Base/SteeringWorld/ParamLoader.h"
// #include "Common/Misc/WindowUtils.h"
#include "Common/Misc/Stream_Utility_Functions.h"
#include "Common/Misc/utils/Utils"


#include "Common/Base/SteeringWorld/resource.h"

#include <list>
using std::list;

#include <cocos2d.h>

#include <Common/Debug/DBGLayer.h>

extern DBGLayer *_g_dbg_layer;

// static float _s_fps;
namespace NSSteeringWorld {
//------------------------------- ctor -----------------------------------
//------------------------------------------------------------------------
SteeringWorld::SteeringWorld(int cx, int cy):
            Scene(),
            m_cxClient(cx),
            m_cyClient(cy),
            m_bPaused(false),
            m_vCrosshair(Vector2D(cxClient()/2.0, cxClient()/2.0)),
            m_bShowWalls(false),
            m_bShowObstacles(false),
            m_bShowPath(false),
            m_bShowWanderCircle(false),
            m_bShowSteeringForce(false),
            m_bShowFeelers(false),
            m_bShowDetectionBox(false),
            m_bShowFPS(true),
            m_dAvFrameTime(0),
            m_pPath(NULL),
            m_bRenderNeighbors(false),
            m_bViewKeys(false),
            m_bShowCellSpaceInfo(false)
{
  //setup the spatial subdivision class
  m_pCellSpace = new CellSpacePartition<Vehicle*>((double)cx, (double)cy, NSSteeringWorld_Prm.NumCellsX, NSSteeringWorld_Prm.NumCellsY, NSSteeringWorld_Prm.NumAgents);

  double border = 30;
  m_pPath = new Path(5, border, border, cx-border, cy-border, true);

  //setup the agents
  for (int a=0; a<NSSteeringWorld_Prm.NumAgents; ++a)
  {

    //determine a random starting position
    Vector2D SpawnPos = Vector2D(cx/2.0+RandomClamped()*cx/2.0,
                                 cy/2.0+RandomClamped()*cy/2.0);

    Vehicle* pVehicle = Vehicle::create(this,
                                    SpawnPos,                 //initial position
                                    RandFloat()*TwoPi,        //start rotation
                                    Vector2D(0,0),            //velocity
                                    NSSteeringWorld_Prm.VehicleMass,          //mass
                                    NSSteeringWorld_Prm.MaxSteeringForce,     //max force
                                    NSSteeringWorld_Prm.MaxSpeed,             //max velocity
                                    NSSteeringWorld_Prm.MaxTurnRatePerSecond, //max turn rate
                                    NSSteeringWorld_Prm.VehicleScale,         //scale
                                    NSSteeringWorld_Prm.Flags,                //flag
                                    "res/icons/maps/ic_navigation.png");        //texture
    pVehicle->setAnchorPoint(cocos2d::Vec2::ANCHOR_MIDDLE);
    pVehicle->setColor(HEX_TO_C3B(GGC_YELLOW_500));
    m_Vehicles.push_back(pVehicle);

    //add it to the cell subdivision
    m_pCellSpace->AddEntity(pVehicle);
  }

  m_Vehicles[NSSteeringWorld_Prm.NumAgents-1]->setScale(1);
#define SHOAL
#ifdef SHOAL
  m_Vehicles[NSSteeringWorld_Prm.NumAgents-1]->Steering()->FlockingOff();
  // m_Vehicles[NSSteeringWorld_Prm.NumAgents-1]->Steering()->WallAvoidanceOn();
  m_Vehicles[NSSteeringWorld_Prm.NumAgents-1]->Steering()->WanderOn();
  m_Vehicles[NSSteeringWorld_Prm.NumAgents-1]->SetMaxSpeed(NSSteeringWorld_Prm.MaxSpeed / 2);
  m_Vehicles[NSSteeringWorld_Prm.NumAgents-1]->setColor(HEX_TO_C3B(GGC_RED_500));
  // __LOGD("m_dBoundingRadius: %.2f : %.2f", m_Vehicles[NSSteeringWorld_Prm.NumAgents-1]->BRadius(), m_Vehicles[NSSteeringWorld_Prm.NumAgents-1]->getBoundingBox().size.width);

   for (int i=0; i<NSSteeringWorld_Prm.NumAgents-1; ++i)
  {

    // m_Vehicles[i]->Steering()->FleeOn();
    // m_Vehicles[i]->Steering()->SeekOn();
    // m_Vehicles[i]->Steering()->ArriveOn();
    // m_Vehicles[i]->Steering()->WanderOn();
    // m_Vehicles[i]->Steering()->CohesionOn();
    // m_Vehicles[i]->Steering()->SeparationOn();
    // m_Vehicles[i]->Steering()->AlignmentOn();
    // m_Vehicles[i]->Steering()->ObstacleAvoidanceOn();
    // m_Vehicles[i]->Steering()->WallAvoidanceOn();
    // m_Vehicles[i]->Steering()->FollowPathOn();
    m_Vehicles[i]->Steering()->FlockingOn();
    // m_Vehicles[i]->Steering()->SetSummingMethod(SteeringBehavior::summing_method::weighted_average);
    m_Vehicles[i]->Steering()->SetSummingMethod(SteeringBehavior::summing_method::prioritized);
    // m_Vehicles[i]->Steering()->SetSummingMethod(SteeringBehavior::summing_method::dithered);
    // m_Vehicles[i]->Steering()->SetPath(m_pPath->GetPath());

    // m_Vehicles[i]->Steering()->PursuitOn(m_Vehicles[NSSteeringWorld_Prm.NumAgents-1]);
    m_Vehicles[i]->Steering()->EvadeOn(m_Vehicles[NSSteeringWorld_Prm.NumAgents-1]);
    // m_Vehicles[i]->Steering()->InterposeOn(m_Vehicles[NSSteeringWorld_Prm.NumAgents-1]);
    // m_Vehicles[i]->Steering()->HideOn(m_Vehicles[NSSteeringWorld_Prm.NumAgents-1]);
    // m_Vehicles[i]->Steering()->OffsetPursuitOn(m_Vehicles[NSSteeringWorld_Prm.NumAgents-1]);
  }

  // for (int i=0; i<NSSteeringWorld_Prm.NumAgents; ++i)
  // {
  //   m_Vehicles[i]->Steering()->WallAvoidanceOn();
  //   m_Vehicles[i]->Steering()->ObstacleAvoidanceOn();
  // }

#endif

  //create any obstacles or walls
  CreateObstacles();
  CreateWalls();
}


//-------------------------------- dtor ----------------------------------
//------------------------------------------------------------------------
SteeringWorld::~SteeringWorld()
{
  // for (unsigned int a=0; a<m_Vehicles.size(); ++a)
  // {
  //   delete m_Vehicles[a];
  // }

  // for (unsigned int ob=0; ob<m_Obstacles.size(); ++ob)
  // {
  //   delete m_Obstacles[ob];
  // }

  delete m_pCellSpace;

  delete m_pPath;
}


//----------------------------- Update -----------------------------------
//------------------------------------------------------------------------
void SteeringWorld::update(float time_elapsed)
{
  if (m_bPaused) return;
  // _s_fps = DIRECTOR->getFrameRate();
  cocos2d::Scene::update(time_elapsed);
  //create a smoother to smooth the framerate
  const int SampleRate = 10;
  static Smoother<double> FrameRateSmoother(SampleRate, 0.0);

  m_dAvFrameTime = FrameRateSmoother.Update(time_elapsed);

  m_dn_dbg->clear();

  //update the vehicles
  for (unsigned int a=0; a<m_Vehicles.size(); ++a)
  {
      m_Vehicles[a]->update(time_elapsed);

      for(const Vector2D &v : m_Vehicles[a]->Steering()->m_pPath->GetPath())
      {
        m_dn_dbg->drawPoint(v, 5, cocos2d::Color4F(HEX_TO_C3B(GGC_GREY_900)));
      }



      // m_dn_dbg->drawLine(m_Vehicles[a]->getPosition(), m_Vehicles[a]->getPosition() + m_Vehicles[a]->Heading() * 20, cocos2d::Color4F(0.0, 1.0, 0.0, 1.0));

      // m_dn_dbg->drawLine(m_Vehicles[a]->getPosition(), m_Vehicles[a]->getPosition() + m_Vehicles[a]->Velocity(), cocos2d::Color4F(1.0, 0.0, 0.0, 0.5f));

      // m_dn_dbg->drawCircle(m_Vehicles[a]->getPosition(), m_Vehicles[a]->BRadius(), 360, 8, false, cocos2d::Color4F(1.f, 1.f, 0, 0.75f));

      // m_dn_dbg->drawRect(m_Vehicles[a]->getBoundingBox().origin, m_Vehicles[a]->getBoundingBox().origin + cocos2d::Vec2(m_Vehicles[a]->getBoundingBox().size.width, m_Vehicles[a]->getBoundingBox().size.height), cocos2d::Color4F(HEX_TO_C3B(GGC_INDIGO_100)));

      // void drawRect(const Vec2 &origin, const Vec2 &destination, const Color4F &color);
  }

  for(size_t _l_i=0; _l_i<m_Obstacles.size(); _l_i++)
  {
    m_dn_dbg->drawCircle(m_Obstacles[_l_i]->getPosition(), m_Obstacles[_l_i]->BRadius(), 360, 8, false, cocos2d::Color4F(HEX_TO_C3B(GGC_INDIGO_900)));
  }

  // this->pause();
}


//--------------------------- CreateWalls --------------------------------
//
//  creates some walls that form an enclosure for the steering agents.
//  used to demonstrate several of the steering behaviors
//------------------------------------------------------------------------
void SteeringWorld::CreateWalls()
{
  //create the walls
  double bordersize = 20.0;
  double CornerSize = 0.2;
  double vDist = m_cyClient-2*bordersize;
  double hDist = m_cxClient-2*bordersize;
  __LOGD("%.2f %.2f", vDist, hDist);
  const int NumWallVerts = 8;

  Vector2D walls[NumWallVerts] = {Vector2D(hDist*CornerSize+bordersize, bordersize),
                                   Vector2D(m_cxClient-bordersize-hDist*CornerSize, bordersize),
                                   Vector2D(m_cxClient-bordersize, bordersize+vDist*CornerSize),
                                   Vector2D(m_cxClient-bordersize, m_cyClient-bordersize-vDist*CornerSize),

                                   Vector2D(m_cxClient-bordersize-hDist*CornerSize, m_cyClient-bordersize),
                                   Vector2D(hDist*CornerSize+bordersize, m_cyClient-bordersize),
                                   Vector2D(bordersize, m_cyClient-bordersize-vDist*CornerSize),
                                   Vector2D(bordersize, bordersize+vDist*CornerSize)};

  for (int w=0; w<NumWallVerts-1; ++w)
  {
    m_Walls.push_back(Wall2D(walls[w], walls[w+1]));
  }

  m_Walls.push_back(Wall2D(walls[NumWallVerts-1], walls[0]));
}


//--------------------------- CreateObstacles -----------------------------
//
//  Sets up the vector of obstacles with random positions and sizes. Makes
//  sure the obstacles do not overlap
//------------------------------------------------------------------------
void SteeringWorld::CreateObstacles()
{
    //create a number of randomly sized tiddlywinks
  for (int o=0; o<NSSteeringWorld_Prm.NumObstacles; ++o)
  {
    bool bOverlapped = true;

    //keep creating tiddlywinks until we find one that doesn't overlap
    //any others.Sometimes this can get into an endless loop because the
    //obstacle has nowhere to fit. We test for this case and exit accordingly

    int NumTrys = 0; int NumAllowableTrys = 2000;

    while (bOverlapped)
    {
      NumTrys++;

      if (NumTrys > NumAllowableTrys) return;

      int radius = RandInt((int)NSSteeringWorld_Prm.MinObstacleRadius, (int)NSSteeringWorld_Prm.MaxObstacleRadius);

      const int border                 = 10;
      const int MinGapBetweenObstacles = 20;

      Obstacle* ob = new Obstacle(RandInt(radius+border, m_cxClient-radius-border),
                                  RandInt(radius+border, m_cyClient-radius-30-border),
                                  radius);

      if (!Overlapped(ob, m_Obstacles, MinGapBetweenObstacles))
      {
        //its not overlapped so we can add it
        m_Obstacles.push_back(ob);

        bOverlapped = false;
      }

      else
      {
        delete ob;
      }
    }
  }
}


//------------------------- Set Crosshair ------------------------------------
//
//  The user can set the position of the crosshair by right clicking the
//  mouse. This method makes sure the click is not inside any enabled
//  Obstacles and sets the position appropriately
//------------------------------------------------------------------------
// void SteeringWorld::SetCrosshair(ccVec2 p)
// {
//   Vector2D ProposedPosition((double)p.x, (double)p.y);

//   //make sure it's not inside an obstacle
//   for (ObIt curOb = m_Obstacles.begin(); curOb != m_Obstacles.end(); ++curOb)
//   {
//     if (PointInCircle((*curOb)->Pos(), (*curOb)->BRadius(), ProposedPosition))
//     {
//       return;
//     }

//   }
//   m_vCrosshair.x = (double)p.x;
//   m_vCrosshair.y = (double)p.y;
// }


//------------------------- HandleKeyPresses -----------------------------
// void SteeringWorld::HandleKeyPresses(WPARAM wParam)
// {

//   switch(wParam)
//   {
//   case 'U':
//     {
//       delete m_pPath;
//       double border = 60;
//       m_pPath = new Path(RandInt(3, 7), border, border, cxClient()-border, cyClient()-border, true);
//       m_bShowPath = true;
//       for (unsigned int i=0; i<m_Vehicles.size(); ++i)
//       {
//         m_Vehicles[i]->Steering()->SetPath(m_pPath->GetPath());
//       }
//     }
//     break;

//     case 'P':

//       TogglePause(); break;

//     case 'O':

//       ToggleRenderNeighbors(); break;

//     case 'I':

//       {
//         for (unsigned int i=0; i<m_Vehicles.size(); ++i)
//         {
//           m_Vehicles[i]->ToggleSmoothing();
//         }

//       }

//       break;

//     case 'Y':

//        m_bShowObstacles = !m_bShowObstacles;

//         if (!m_bShowObstacles)
//         {
//           m_Obstacles.clear();

//           for (unsigned int i=0; i<m_Vehicles.size(); ++i)
//           {
//             m_Vehicles[i]->Steering()->ObstacleAvoidanceOff();
//           }
//         }
//         else
//         {
//           CreateObstacles();

//           for (unsigned int i=0; i<m_Vehicles.size(); ++i)
//           {
//             m_Vehicles[i]->Steering()->ObstacleAvoidanceOn();
//           }
//         }
//         break;

//   }//end switch
// }



//-------------------------- HandleMenuItems -----------------------------
// void SteeringWorld::HandleMenuItems(WPARAM wParam, HWND hwnd)
// {
//   switch(wParam)
//   {
//     case ID_OB_OBSTACLES:

//         m_bShowObstacles = !m_bShowObstacles;

//         if (!m_bShowObstacles)
//         {
//           m_Obstacles.clear();

//           for (unsigned int i=0; i<m_Vehicles.size(); ++i)
//           {
//             m_Vehicles[i]->Steering()->ObstacleAvoidanceOff();
//           }

//           //uncheck the menu
//          ChangeMenuState(hwnd, ID_OB_OBSTACLES, MFS_UNCHECKED);
//         }
//         else
//         {
//           CreateObstacles();

//           for (unsigned int i=0; i<m_Vehicles.size(); ++i)
//           {
//             m_Vehicles[i]->Steering()->ObstacleAvoidanceOn();
//           }

//           //check the menu
//           ChangeMenuState(hwnd, ID_OB_OBSTACLES, MFS_CHECKED);
//         }

//        break;

//     case ID_OB_WALLS:

//       m_bShowWalls = !m_bShowWalls;

//       if (m_bShowWalls)
//       {
//         CreateWalls();

//         for (unsigned int i=0; i<m_Vehicles.size(); ++i)
//         {
//           m_Vehicles[i]->Steering()->WallAvoidanceOn();
//         }

//         //check the menu
//          ChangeMenuState(hwnd, ID_OB_WALLS, MFS_CHECKED);
//       }

//       else
//       {
//         m_Walls.clear();

//         for (unsigned int i=0; i<m_Vehicles.size(); ++i)
//         {
//           m_Vehicles[i]->Steering()->WallAvoidanceOff();
//         }

//         //uncheck the menu
//          ChangeMenuState(hwnd, ID_OB_WALLS, MFS_UNCHECKED);
//       }

//       break;


//     case IDR_PARTITIONING:
//       {
//         for (unsigned int i=0; i<m_Vehicles.size(); ++i)
//         {
//           m_Vehicles[i]->Steering()->ToggleSpacePartitioningOnOff();
//         }

//         //if toggled on, empty the cell space and then re-add all the
//         //vehicles
//         if (m_Vehicles[0]->Steering()->isSpacePartitioningOn())
//         {
//           m_pCellSpace->EmptyCells();

//           for (unsigned int i=0; i<m_Vehicles.size(); ++i)
//           {
//             m_pCellSpace->AddEntity(m_Vehicles[i]);
//           }

//           ChangeMenuState(hwnd, IDR_PARTITIONING, MFS_CHECKED);
//         }
//         else
//         {
//           ChangeMenuState(hwnd, IDR_PARTITIONING, MFS_UNCHECKED);
//           ChangeMenuState(hwnd, IDM_PARTITION_VIEW_NEIGHBORS, MFS_UNCHECKED);
//           m_bShowCellSpaceInfo = false;

//         }
//       }

//       break;

//     case IDM_PARTITION_VIEW_NEIGHBORS:
//       {
//         m_bShowCellSpaceInfo = !m_bShowCellSpaceInfo;

//         if (m_bShowCellSpaceInfo)
//         {
//           ChangeMenuState(hwnd, IDM_PARTITION_VIEW_NEIGHBORS, MFS_CHECKED);

//           if (!m_Vehicles[0]->Steering()->isSpacePartitioningOn())
//           {
//             SendMessage(hwnd, WM_COMMAND, IDR_PARTITIONING, NULL);
//           }
//         }
//         else
//         {
//           ChangeMenuState(hwnd, IDM_PARTITION_VIEW_NEIGHBORS, MFS_UNCHECKED);
//         }
//       }
//       break;


//     case IDR_WEIGHTED_SUM:
//       {
//         ChangeMenuState(hwnd, IDR_WEIGHTED_SUM, MFS_CHECKED);
//         ChangeMenuState(hwnd, IDR_PRIORITIZED, MFS_UNCHECKED);
//         ChangeMenuState(hwnd, IDR_DITHERED, MFS_UNCHECKED);

//         for (unsigned int i=0; i<m_Vehicles.size(); ++i)
//         {
//           m_Vehicles[i]->Steering()->SetSummingMethod(SteeringBehavior::weighted_average);
//         }
//       }

//       break;

//     case IDR_PRIORITIZED:
//       {
//         ChangeMenuState(hwnd, IDR_WEIGHTED_SUM, MFS_UNCHECKED);
//         ChangeMenuState(hwnd, IDR_PRIORITIZED, MFS_CHECKED);
//         ChangeMenuState(hwnd, IDR_DITHERED, MFS_UNCHECKED);

//         for (unsigned int i=0; i<m_Vehicles.size(); ++i)
//         {
//           m_Vehicles[i]->Steering()->SetSummingMethod(SteeringBehavior::prioritized);
//         }
//       }

//       break;

//     case IDR_DITHERED:
//       {
//         ChangeMenuState(hwnd, IDR_WEIGHTED_SUM, MFS_UNCHECKED);
//         ChangeMenuState(hwnd, IDR_PRIORITIZED, MFS_UNCHECKED);
//         ChangeMenuState(hwnd, IDR_DITHERED, MFS_CHECKED);

//         for (unsigned int i=0; i<m_Vehicles.size(); ++i)
//         {
//           m_Vehicles[i]->Steering()->SetSummingMethod(SteeringBehavior::dithered);
//         }
//       }

//       break;


//       case ID_VIEW_KEYS:
//       {
//         ToggleViewKeys();

//         // CheckMenuItemAppropriately(hwnd, ID_VIEW_KEYS, m_bViewKeys);
//       }

//       break;

//       case ID_VIEW_FPS:
//       {
//         ToggleShowFPS();

//         // CheckMenuItemAppropriately(hwnd, ID_VIEW_FPS, RenderFPS());
//       }

//       break;

//       case ID_MENU_SMOOTHING:
//       {
//         for (unsigned int i=0; i<m_Vehicles.size(); ++i)
//         {
//           m_Vehicles[i]->ToggleSmoothing();
//         }

//         // CheckMenuItemAppropriately(hwnd, ID_MENU_SMOOTHING, m_Vehicles[0]->isSmoothingOn());
//       }

//       break;

//   }//end switch
// }


//------------------------------ Render ----------------------------------
//------------------------------------------------------------------------
// void SteeringWorld::Render()
// {
//   gdi->TransparentText();

//   //render any walls
//   gdi->BlackPen();
//   for (unsigned int w=0; w<m_Walls.size(); ++w)
//   {
//     m_Walls[w].Render(true);  //true flag shows normals
//   }

//   //render any obstacles
//   gdi->BlackPen();

//   for (unsigned int ob=0; ob<m_Obstacles.size(); ++ob)
//   {
//     gdi->Circle(m_Obstacles[ob]->Pos(), m_Obstacles[ob]->BRadius());
//   }

//   //render the agents
//   for (unsigned int a=0; a<m_Vehicles.size(); ++a)
//   {
//     m_Vehicles[a]->Render();

//     //render cell partitioning stuff
//     if (m_bShowCellSpaceInfo && a==0)
//     {
//       gdi->HollowBrush();
//       InvertedAABBox2D box(m_Vehicles[a]->Pos() - Vector2D(NSSteeringWorld_Prm.ViewDistance, NSSteeringWorld_Prm.ViewDistance),
//                            m_Vehicles[a]->Pos() + Vector2D(NSSteeringWorld_Prm.ViewDistance, NSSteeringWorld_Prm.ViewDistance));
//       box.Render();

//       gdi->RedPen();
//       CellSpace()->CalculateNeighbors(m_Vehicles[a]->Pos(), NSSteeringWorld_Prm.ViewDistance);
//       for (BaseGameEntity* pV = CellSpace()->begin();!CellSpace()->end();pV = CellSpace()->next())
//       {
//         gdi->Circle(pV->Pos(), pV->BRadius());
//       }

//       gdi->GreenPen();
//       gdi->Circle(m_Vehicles[a]->Pos(), NSSteeringWorld_Prm.ViewDistance);
//     }
//   }

// //#define CROSSHAIR
// #ifdef CROSSHAIR
//   //and finally the crosshair
//   gdi->RedPen();
//   gdi->Circle(m_vCrosshair, 4);
//   gdi->Line(m_vCrosshair.x - 8, m_vCrosshair.y, m_vCrosshair.x + 8, m_vCrosshair.y);
//   gdi->Line(m_vCrosshair.x, m_vCrosshair.y - 8, m_vCrosshair.x, m_vCrosshair.y + 8);
//   gdi->TextAtPos(5, cyClient() - 20, "Click to move crosshair");
// #endif


//   //gdi->TextAtPos(cxClient() -120, cyClient() - 20, "Press R to reset");

//   gdi->TextColor(Cgdi::grey);
//   if (RenderPath())
//   {
//      gdi->TextAtPos((int)(cxClient()/2.0f - 80), cyClient() - 20, "Press 'U' for random path");

//      m_pPath->Render();
//   }

//   if (RenderFPS())
//   {
//     gdi->TextColor(Cgdi::grey);
//     gdi->TextAtPos(5, cyClient() - 20, ttos(1.0 / m_dAvFrameTime));
//   }

//   if (m_bShowCellSpaceInfo)
//   {
//     m_pCellSpace->RenderCells();
//   }

// }

bool SteeringWorld::init()
{
  if(!cocos2d::Scene::init()) return false;

  _m_layer_action = cocos2d::Layer::create();
  this->addChild(_m_layer_action);

  cocos2d::LayerColor *_l_layer_color = cocos2d::LayerColor::create(cocos2d::Color4B(HEX_TO_C3B(GGC_BLUE_GREY_500)), SCREEN_X(1.f),SCREEN_Y(1.f));
  _m_layer_action->addChild(_l_layer_color, -1);

  for(auto _l_vehicle : m_Vehicles)
  {
    __LOGD("");
    _m_layer_action->addChild(_l_vehicle);
  }

  m_dn_dbg = cocos2d::DrawNode::create();
  this->addChild(m_dn_dbg, 1000);

  scheduleUpdate();
  // schedule(CC_SCHEDULE_SELECTOR(SteeringWorld::update), 3.f);

  return true;
}

}

cocos2d::Scene *getScene()
{
    return NSSteeringWorld::SteeringWorld::createScene();
}