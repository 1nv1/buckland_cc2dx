#include "ParamLoader.h"

NSSteeringWorld::ParamLoader* NSSteeringWorld::ParamLoader::Instance()
{
  static ParamLoader instance;

  return &instance;
}