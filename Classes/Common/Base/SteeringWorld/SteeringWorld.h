#ifndef __STEERING_WORLD_H__
#define __STEERING_WORLD_H__
#pragma warning (disable:4786)
//------------------------------------------------------------------------
//
//  Name:   SteeringWorld.h
//
//  Desc:   All the environment data and methods for the Steering
//          Behavior projects. This class is the root of the project's
//          update and render calls (excluding main of course)
//
//  Author: Mat Buckland 2002 (fup@ai-junkie.com)
//
//------------------------------------------------------------------------
// #include <windows.h>
#include <vector>

#include "Common/2D/Vector2D.h"
#include "Common/Time/PrecisionTimer.h"
#include "Common/Misc/CellSpacePartition.h"
#include "Common/Base/BaseGameEntity.h"
#include "Common/Base/EntityFunctionTemplates.h"
#include "Common/Base/SteeringWorld/Vehicle.h"

#include <cocos2d.h>

#include "Common/Misc/utils/Utils"

class Wall2D;
class Path;

typedef std::vector<BaseGameEntity*>::iterator  ObIt;

namespace NSSteeringWorld {

class Vehicle;
class Obstacle;

class SteeringWorld : public cocos2d::Scene
{
private:

  //a container of all the moving entities
  std::vector<Vehicle*>         m_Vehicles;

  //any obstacles
  std::vector<BaseGameEntity*>  m_Obstacles;

  //container containing any walls in the environment
  std::vector<Wall2D>           m_Walls;

  CellSpacePartition<Vehicle*>* m_pCellSpace;

  //any path we may create for the vehicles to follow
  Path*                         m_pPath;

  //set true to pause the motion
  bool                          m_bPaused;

  //local copy of client window dimensions
  int                           m_cxClient,
                                m_cyClient;
  //the position of the crosshair
  Vector2D                      m_vCrosshair;

  //keeps track of the average FPS
  double                        m_dAvFrameTime;


  //flags to turn aids and obstacles etc on/off
  bool  m_bShowWalls;
  bool  m_bShowObstacles;
  bool  m_bShowPath;
  bool  m_bShowDetectionBox;
  bool  m_bShowWanderCircle;
  bool  m_bShowFeelers;
  bool  m_bShowSteeringForce;
  bool  m_bShowFPS;
  bool  m_bRenderNeighbors;
  bool  m_bViewKeys;
  bool  m_bShowCellSpaceInfo;


  void CreateObstacles();

  void CreateWalls();



public:

  SteeringWorld(int cx, int cy);

  ~SteeringWorld();

  void  update(float time_elapsed) override;

  // void  Render();


  void  NonPenetrationContraint(Vehicle* v){EnforceNonPenetrationConstraint(v, m_Vehicles);}

  void  TagVehiclesWithinViewRange(BaseGameEntity* pVehicle, double range)
  {
    TagNeighbors(pVehicle, m_Vehicles, range);
  }

  void  TagObstaclesWithinViewRange(BaseGameEntity* pVehicle, double range)
  {
    TagNeighbors(pVehicle, m_Obstacles, range);
  }

  const std::vector<Wall2D>&          Walls(){return m_Walls;}
  CellSpacePartition<Vehicle*>*       CellSpace(){return m_pCellSpace;}
  const std::vector<BaseGameEntity*>& Obstacles()const{return m_Obstacles;}
  const std::vector<Vehicle*>&        Agents(){return m_Vehicles;}


  //handle WM_COMMAND messages
  // void        HandleKeyPresses(WPARAM wParam);
  // void        HandleMenuItems(WPARAM wParam, HWND hwnd);

  void        TogglePause(){m_bPaused = !m_bPaused;}
  bool        Paused()const{return m_bPaused;}

  Vector2D    Crosshair()const{return m_vCrosshair;}
  // void        SetCrosshair(ccVec2 p);
  void        SetCrosshair(Vector2D v){m_vCrosshair=v;}

  int   cxClient()const{return m_cxClient;}
  int   cyClient()const{return m_cyClient;}

  bool  RenderWalls()const{return m_bShowWalls;}
  bool  RenderObstacles()const{return m_bShowObstacles;}
  bool  RenderPath()const{return m_bShowPath;}
  bool  RenderDetectionBox()const{return m_bShowDetectionBox;}
  bool  RenderWanderCircle()const{return m_bShowWanderCircle;}
  bool  RenderFeelers()const{return m_bShowFeelers;}
  bool  RenderSteeringForce()const{return m_bShowSteeringForce;}

  bool  RenderFPS()const{return m_bShowFPS;}
  void  ToggleShowFPS(){m_bShowFPS = !m_bShowFPS;}

  void  ToggleRenderNeighbors(){m_bRenderNeighbors = !m_bRenderNeighbors;}
  bool  RenderNeighbors()const{return m_bRenderNeighbors;}

  void  ToggleViewKeys(){m_bViewKeys = !m_bViewKeys;}
  bool  ViewKeys()const{return m_bViewKeys;}

public:
  // from cc2dx
  static inline cocos2d::Scene* createScene()
  {
    return SteeringWorld::create(static_cast<int>(SCREEN_X(1.f)), static_cast<int>(SCREEN_Y(1.f)));
  }

  // CREATE_FUNC(SteeringWorld);
  static inline SteeringWorld *create(int cx, int cy)
    {
        SteeringWorld *ret = new (std::nothrow) SteeringWorld(cx, cy);
        if (ret && ret->init())
        {
            ret->autorelease();
            return ret;
        }
        else
        {
            CC_SAFE_DELETE(ret);
            return nullptr;
        }
    }

    virtual bool init() override;

    cocos2d::DrawNode *m_dn_dbg;
    cocos2d::Layer *_m_layer_action;
};

}

#endif