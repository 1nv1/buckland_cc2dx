#ifndef OBSTACLE_H
#define OBSTACLE_H
//------------------------------------------------------------------------
//
//  Name:   Obstacle.h
//
//  Desc:   Simple obstacle class
//
//  Author: Mat Buckland 2002 (fup@ai-junkie.com)
//
//------------------------------------------------------------------------
#include "Common/2D/Vector2D.h"
#include "Common/Base/BaseGameEntity.h"
// #include "Common/Misc/Cgdi.h"


// #include <windows.h>
namespace NSSteeringWorld {

class Obstacle : public BaseGameEntity
{
public:

  Obstacle(double x,
           double y,
           float r):BaseGameEntity(BaseGameEntity::GetNextValidID(), Vector2D(x,y), r)
  {}

  Obstacle(Vector2D pos, float radius):BaseGameEntity(BaseGameEntity::GetNextValidID(), pos, radius)
  {}

  Obstacle(std::ifstream& in){Read(in);}

  virtual ~Obstacle(){}

  //this is defined as a pure virtual function in BasegameEntity so
  //it must be implemented
  virtual void      update(float time_elapsed) override {}

  // void      Render(){gdi->BlackPen();gdi->Circle(Pos(), BRadius());}

  void      Write(std::ostream& os)const;
  void      Read(std::ifstream& in);
};

}

#endif

